<?php

use Illuminate\Database\Seeder;

class PlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0');
        \DB::table('plans')->truncate();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1');

        \DB::table('plans')->insert(
            [
                [
                    'type' => "RECURRING",
                    'name' => 'Loyalty Stater',
                    'price' => 00.00,
                    'capped_amount' => 0.00,
                    'terms' => 'Free Plan',
                    'trial_days' => 0,
                    'test' => 1,
                    'on_install' => 0
                ],
                [
                    'type' => "RECURRING",
                    'name' => 'Online Loyalty',
                    'price' => 49.00,
                    'capped_amount' => 0.00,
                    'terms' => 'Perfect for online store',
                    'trial_days' => 0,
                    'test' => 1,
                    'on_install' => 0
                ],
                [
                    'type' => "RECURRING",
                    'name' => 'Online and Offline',
                    'price' => 199.00,
                    'capped_amount' => 0.00,
                    'terms' => 'Perfect for physical stores with online presence',
                    'trial_days' => 0,
                    'test' => 1,
                    'on_install' => 0
                ],
                [
                    'type' => "RECURRING",
                    'name' => 'Enterprise',
                    'price' => 499.00,
                    'capped_amount' => 0.00,
                    'terms' => 'For more 5+ store and heavy online presence',
                    'trial_days' => 0,
                    'test' => 1,
                    'on_install' => 0
                ],
            ]
        );
    }
}
