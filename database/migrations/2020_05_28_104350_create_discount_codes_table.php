<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiscountCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discount_codes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('discount_id')->nullable();
            $table->string('discount_code')->nullable();
            $table->string('summary')->nullable();
            $table->json('response')->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete("CASCADE")->onUpdate("No ACTION");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discount_codes');
    }
}
