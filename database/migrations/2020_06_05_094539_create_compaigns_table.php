<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns', function (Blueprint $table) {
            $table->id();

            /* Step 1  */
            $table->unsignedBigInteger('user_id');
            $table->string('type',10)->nullable()->comment("campaign, offer");
            $table->boolean('status')->default(1)->comment("1:enable, 0:disable");
            $table->string('label')->nullable();
            $table->dateTime('start_at')->nullable();
            $table->dateTime('end_at')->nullable();

            /* Step 2 */
            $table->unsignedBigInteger('customer_level_id');
            $table->tinyInteger('customer_age_from')->nullable();
            $table->tinyInteger('customer_age_to')->nullable();
            $table->string('customer_gender',10)->nullable()->comment("male, female, other");
            $table->unsignedBigInteger('earning_point_id')->nullable();

            /* Step 3  */
            $table->decimal('require_points',10,2)->default(0);
            $table->unsignedBigInteger('discount_id')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete("CASCADE")->onUpdate("No ACTION");

            $table->foreign('customer_level_id')->references('id')->on('customer_levels')->onDelete("CASCADE")->onUpdate("No ACTION");

            $table->foreign('earning_point_id')->references('id')->on('earning_points')->onDelete("CASCADE")->onUpdate("No ACTION");

            $table->foreign('discount_id')->references('id')->on('discount_codes')->onDelete("CASCADE")->onUpdate("No ACTION");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaigns');
    }
}
