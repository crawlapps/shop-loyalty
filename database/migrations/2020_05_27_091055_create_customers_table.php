<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->date('dob')->nullable();
            $table->enum('gender',['male','female','other'])->nullable();
            $table->decimal('points',10,2)->default(0);
            $table->decimal('spent',10,2)->default(0);
            $table->decimal('balance',10,2)->default(0);
            $table->string('loyalty_number')->nullable();
            $table->string('card_barcode')->nullable();
            $table->string('card_barcode_1')->nullable();
            $table->dateTime('start_date')->nullable();

            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete("CASCADE")->onUpdate("No ACTION");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
