<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDisplaySettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('display_settings', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');

            $table->string('widget_text')->nullable();
            $table->string('logo_path')->nullable();
            $table->json('common_data')->nullable();

            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete("CASCADE")->onUpdate("No ACTION");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('display_settings');
    }
}
