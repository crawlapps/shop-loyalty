<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerEarningPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_earning_points', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('earning_point_id');
            $table->unsignedBigInteger('customer_id');
            $table->float('points');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete("CASCADE")->onUpdate("No ACTION");
            $table->foreign('earning_point_id')->references('id')->on('earning_points')->onDelete("CASCADE")->onUpdate("No ACTION");
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete("CASCADE")->onUpdate("No ACTION");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_earning_points');
    }
}
