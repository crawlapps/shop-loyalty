<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssignedCustomerLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assigned_customer_levels', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('customer_level_id');
            $table->unsignedBigInteger('customer_id');
            $table->string('discount_id')->nullable();
            $table->string('discount_code')->nullable();
            $table->boolean('status')->default(1)->comment('0: Deactive; 1: Active');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete("CASCADE")->onUpdate("No ACTION");
             $table->foreign('customer_level_id')->references('id')->on('customer_levels')->onDelete("CASCADE")->onUpdate("No ACTION");
             $table->foreign('customer_id')->references('id')->on('customers')->onDelete("CASCADE")->onUpdate("No ACTION");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assigned_customer_levels');
    }
}
