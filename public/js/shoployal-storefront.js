/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/shoployal-storefront.js":
/*!**********************************************!*\
  !*** ./resources/js/shoployal-storefront.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var shoployal_base_url = "https://shop-loyalty.test";
var apiEndPoint = shoployal_base_url + '/api/';
var shopifyDomain = Shopify.shop;
var frontendDomain = window.location.origin;
var jqueryLoaded = 0;

if (!window.jQuery) {
  var script = document.createElement('script');
  script.type = "text/javascript";
  script.src = "https://code.jquery.com/jquery-3.5.1.min.js";
  document.getElementsByTagName('head')[0].appendChild(script);
  jqueryLoaded = 1;
}

var cus_attached_params = "",
    url = "",
    base = "";
Window.shoployalStoreFront = {
  shop_data: JSON.parse(document.getElementById('crawlapps_shoployal_data').innerHTML),
  init: function init() {
    base = this;
    var myParam = getParameterByName('_loy', window.location.href);

    if (myParam == "_sty128Rtyse7I5uY") {
      var inputTag = document.createElement('input');
      inputTag.type = "hidden";
      inputTag.name = "customer[tags]";
      inputTag.value = "shopLoyal-customer";
      var form = document.querySelector("form[action='/account']");
      form.appendChild(inputTag);
    }

    var current_url = window.location.href;

    if (current_url.indexOf('uniqueId') > 0) {
      var pieces = current_url.split('uniqueId=');

      if (pieces[1] && pieces[1] != '') {
        setCookie(pieces[1]);
      }
    }

    var shopURL = window.location.host;
    var iframe = document.createElement('iframe');
    iframe.id = "shoployal-frame";
    iframe.style.position = "fixed";
    iframe.style.right = "0px";
    iframe.style.bottom = "0px";
    iframe.style.zIndex = "10";
    iframe.style.height = "100px";
    iframe.style.width = "240px";
    iframe.style.border = "none";

    if (typeof __st.cid != 'undefined') {
      url = apiEndPoint + shopifyDomain + '/widget/dashboard?customer=' + __st.cid + '&required=layout';
      cus_attached_params = '?shop=' + shopURL + '&customer=' + __st.cid + "&is_login=1";
    } else {
      url = apiEndPoint + shopifyDomain + '/widget?is_login=0';
      cus_attached_params = '?shop=' + shopifyDomain;
    }

    document.body.appendChild(iframe);
    base.initFrame();
  },
  initFrame: function initFrame() {
    if (typeof __st.cid !== 'undefined' && getCookie("SL_unique_id") !== "") {
      //let cust_url = aPIEndPoint + shopifyDomain + "/save-app-user"+cus_attached_params+"&unique_id=" + getCookie("SL_unique_id")
      $.ajax({
        method: "GET",
        url: url,
        contentType: 'application/json;',
        success: function success(response, _success, header) {
          document.cookie = "SL_unique_id" + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';

          if (response != '') {
            var iframeDoc = $('#shoployal-frame')[0].contentDocument;
            iframeDoc.write(response);
          }
        },
        error: function error(XMLHttpRequest, textStatus, errorThrown) {
          console.log(errorThrown);
        }
      });
    }

    $.ajax({
      url: url,
      success: function success(result) {
        var iframeDoc = $('#shoployal-frame')[0].contentDocument;
        iframeDoc.write(result);
      }
    });
  }
};

if (jqueryLoaded) {
  setTimeout(function () {
    $(document).ready(function () {
      Window.shoployalStoreFront.init();
    });
  }, 2000);
} else {
  $(document).ready(function () {
    Window.shoployalStoreFront.init();
  });
}

function getCookie(name) {
  var value = "; " + document.cookie;
  var parts = value.split("; " + name + "=");
  if (parts.length == 2) return parts.pop().split(";").shift();
  return "";
}

function setCookie(value) {
  var exdate = new Date();
  exdate.setDate(exdate.getDate() + 1);
  var c_value = escape(value) + "; expires=" + exdate.toUTCString() + "; path=/";
  document.cookie = "SL_unique_id" + "=" + c_value;
}

function getParameterByName(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, '\\$&');
  var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
      results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

/***/ }),

/***/ 1:
/*!****************************************************!*\
  !*** multi ./resources/js/shoployal-storefront.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/vishalgohil/projects/ofc/shop-loyalty/resources/js/shoployal-storefront.js */"./resources/js/shoployal-storefront.js");


/***/ })

/******/ });