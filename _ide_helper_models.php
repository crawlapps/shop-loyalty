<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\EarningPoint
 *
 * @property int $id
 * @property int $user_id
 * @property string $label
 * @property float $points
 * @property int $status 1: Enable; 0: Disable
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\User $belongs_to_user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EarningPoint newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EarningPoint newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EarningPoint query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EarningPoint whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EarningPoint whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EarningPoint whereLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EarningPoint wherePoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EarningPoint whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EarningPoint whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EarningPoint whereUserId($value)
 */
	class EarningPoint extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property int $status 0: Disable; 1: Enable;
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $shopify_grandfathered
 * @property string|null $shopify_namespace
 * @property int $shopify_freemium
 * @property int|null $plan_id
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Osiset\ShopifyApp\Storage\Models\Charge[] $charges
 * @property-read int|null $charges_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\EarningPoint[] $has_many_earning
 * @property-read int|null $has_many_earning_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Osiset\ShopifyApp\Storage\Models\Plan|null $plan
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePlanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereShopifyFreemium($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereShopifyGrandfathered($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereShopifyNamespace($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 */
	class User extends \Eloquent implements \Osiset\ShopifyApp\Contracts\ShopModel {}
}

