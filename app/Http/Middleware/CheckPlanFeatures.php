<?php

namespace App\Http\Middleware;

use Closure;

class CheckPlanFeatures
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!checkPlanFeatures()['customers']){
            return response(["message" => "You have reached the limit Please upgrade your plan"], 400);;
        }


        return $next($request);
    }
}
