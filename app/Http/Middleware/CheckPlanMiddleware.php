<?php

namespace App\Http\Middleware;

use Closure;

class CheckPlanMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $shop = \Auth::user();

        if(is_null($shop->plan_id) && !$shop->isGrandfathered() && !$shop->isFreemium()){
            return redirect("/choose-plan");
        }


        return $next($request);
    }
}
