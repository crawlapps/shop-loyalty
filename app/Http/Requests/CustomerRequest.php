<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Route;


class CustomerRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = @$this->id;
        $shop_id = getShop()->id;

        if(empty($id)) {
            $id = null;
        }

        switch (Route::currentRouteName()) {
            case "customers.store":
                {
                    $rule = [
                        "name" => "required",
                        'email' => "required|email|unique:customers,email,{$id},id,user_id,{$shop_id}",
                        'dob' => "required"
                    ];
                    return $rule;
                }
            case "customers.update":
                {
                    $rule = [
                        "name" => "required",
                        'email' => "required|email|unique:customers,email,{$id},id,user_id,{$shop_id}",
                        'dob' => "required"
                    ];
                    return $rule;
                }
            default:
                break;
        }
    }

    public function messages(){
        return [

        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        if ($this->ajax() || $this->wantsJson()) {
            $response = new JsonResponse($validator->errors(), 422);
            throw new ValidationException($validator, $response);
        }

        throw (new ValidationException($validator))
            ->errorBag($this->errorBag)
            ->redirectTo($this->getRedirectUrl());
    }
}
