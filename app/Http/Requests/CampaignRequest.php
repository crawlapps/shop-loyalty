<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Route;


class CampaignRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule = [
            "label" => "required",
            'start_at' => "required|date_format:Y-m-d",
            'end_at' => "nullable|date_format:Y-m-d",
            'customer_level_id' => "required",
            //'earning_point_id' => "required",
            'require_points' => "required",
            //'discount_id' => "required",
        ];

        switch (Route::currentRouteName()) {
            case "campaign.store":
                {

                    return $rule;
                }
            case "campaign.update":
                {
                    return $rule;
                }
            default:
                break;
        }
    }

    public function messages(){
        return [
            'label.required' => "The campaign name field is required.",
            'discount_id.required' => "The discount code field is required.",
            'earning_point_id.required' => "The earning points field is required.",
            'customer_level_id.required' => "The customer level field is required.",
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        if ($this->ajax() || $this->wantsJson()) {
            $response = new JsonResponse($validator->errors(), 422);
            throw new ValidationException($validator, $response);
        }

        throw (new ValidationException($validator))
            ->errorBag($this->errorBag)
            ->redirectTo($this->getRedirectUrl());
    }
}
