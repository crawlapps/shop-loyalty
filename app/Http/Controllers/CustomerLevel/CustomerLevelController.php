<?php

namespace App\Http\Controllers\CustomerLevel;

use App\Http\Controllers\Controller;
use App\Http\Requests\CustomerLevelRequest;
use App\Models\CustomerLevel;
use App\Models\DiscountCode;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CustomerLevelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->filled('api'))
            return $this->apiIndex($request);
        return view('pages.customer-level.index');
    }

    public function apiIndex($request){
        if($request->filled('type')) {
            return $this->initChart();
        }

        $per_page = ($request->filled('per_page'))?$request->per_page:15;
        $customers = CustomerLevel::whereUserId(getShop()->id)->orderBy('label','asc')->paginate($per_page);
        return response($customers,200);
    }

    public function initChart(){
        $customer_level = CustomerLevel::withCount('has_many_assigned_customer as count')->whereUserId(getShop()->id)->get();
        return response($customer_level,Response::HTTP_OK);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CustomerLevelRequest $request)
    {
        $customer_level = new CustomerLevel;
        $customer_level->user_id = getShop()->id;
        $customer_level->label = $request->label;
        $customer_level->discount_id = $request->discount_id;
        $customer_level->require_points = $request->require_points;
        $customer_level->save();

        $discount = DiscountCode::find($request->discount_id);
        $discount->used_on = config('constants.customer_level');
        $discount->save();

        return response(['message' => "Customer Level successfully created."], Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(CustomerLevel $customerLevel)
    {
        return response($customerLevel,200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CustomerLevel $customer_level)
    {
        $customer_level->label = $request->label;
        $customer_level->discount_id = $request->discount_id;
        $customer_level->require_points = $request->require_points;
        $customer_level->save();

        return response(['message' => "Customer Level successfully updated."], Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
