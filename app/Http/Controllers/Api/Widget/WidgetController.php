<?php

namespace App\Http\Controllers\Api\Widget;

use App\Http\Controllers\Controller;
use App\Models\AssignedCustomerLevel;
use App\Models\Campaign;
use App\Models\Customer;
use App\Models\CustomerEarningPoint;
use App\Models\CustomerLevel;
use App\Models\DiscountCode;
use App\Models\DisplaySetting;
use App\Models\EarningPoint;
use App\Notifications\UserVisitNotification;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class WidgetController extends Controller
{
    public function index(Request $request, $shopifyShop) {
        $shop = User::whereName($shopifyShop)->firstOrFail();
        $settings = DisplaySetting::whereUserId($shop->id)->firstOrFail();

        $is_login = 0;
        if($request->is_login)
            $is_login = 1;

        return view("pages.widget.index",compact('settings', 'shop', 'is_login'));
    }

    public function dashboard(Request $request, $shopifyShop){
        $shop = User::whereName($shopifyShop)->firstOrFail();
        $customer_id = $request->customer;
        $customer = Customer::whereCustomerId($customer_id)->whereUserId($shop->id)->first();
        $settings = DisplaySetting::whereUserId($shop->id)->firstOrFail();
        $forJoining = 0;
        $is_login = 1;
        $current_level = null;
        if(!$customer){
            $forJoining = 1;
        }else{
            $earningPoint = EarningPoint::whereStatus(1)->whereSlug("visits")->whereUserId($shop->id)->first();
            if($earningPoint){
                $customer_ep = CustomerEarningPoint::whereCustomerId($customer->id)
                    ->whereUserId($shop->id)
                    ->whereEarningPointId($earningPoint->id)
                    ->whereDate('created_at',Carbon::today())
                    ->first();
                if(!$customer_ep){
                    storeCustomerEarningPoints($customer, $earningPoint, $shop);
                    $customer_ep = CustomerEarningPoint::whereCustomerId($customer->id)
                        ->whereUserId($shop->id)
                        ->whereEarningPointId($earningPoint->id);
                    if($customer_ep->get()->count() <= 1) {
                        $extra = [];
                        $user = new User();
                        $user->email = $customer->email;
                        $user->name = $customer->name;
                        $extra['points'] = $earningPoint->points;
                        $extra['store_name'] = $shop->store_name;
                        \Notification::send($user, new UserVisitNotification($extra, $user));
                    }

                }
            }
            $current_level = AssignedCustomerLevel::whereUserId($shop->id)->whereCustomerId($customer->id)->whereStatus(1)->first();
        }
        $page = 'my-info';
        return view("pages.widget.index",compact('settings', 'shop', 'is_login','forJoining','customer_id','customer','current_level','page'));
    }

    public function customerJoin(Request $request, $shopifyShop){
        $shop = User::whereName($shopifyShop)->firstOrFail();
        $settings = DisplaySetting::whereUserId($shop->id)->firstOrFail();
        $customer_id = $request->customer;

        $is_login = 1;
        $forJoining = 0;


        $customer = $shop->api()->rest('GET',"/admin/customers/{$customer_id}.json");

        if(!$customer->errors){
            $joined = 1;
            $data = $customer->bodyArray['customer'];
            $customer = new Customer;
            $customer->user_id = $shop->id;
            $customer->customer_id = $data['id'];
            $customer->name = $data['first_name']." ".$data['last_name'];
            $customer->email = $data['email'];
            $customer->loyalty_number = $request->barcode;
            $customer->start_date = date("Y-m-d H:i:s");
            $customer->save();
            return view("pages.widget.index",compact('settings', 'shop', 'is_login','joined','forJoining','customer_id', 'customer'));
        }
    }

    public function customerDiscount(Request $request, $shopifyShop){
        $forJoining = 0;
        $is_login = 1;
        $shop = User::whereName($shopifyShop)->firstOrFail();
        $customer_id = $request->customer;
        $customer = Customer::whereCustomerId($customer_id)->whereUserId($shop->id)->first();
        if ($request->isMethod('post')) {
            $customer->discount_id=$request->selected_discount;
            $customer->save();
        }


        $current_level = null;
        $settings = DisplaySetting::whereUserId($shop->id)->firstOrFail();

        $assignedLevel = CustomerLevel::whereUserId($shop->id)->whereHas('has_many_assigned_customer', function($q) use ($customer) {
            $q->whereCustomerId($customer->id)->whereStatus(1);
        })->get(['discount_id'])->pluck('discount_id')->toArray();

        $campaigns = Campaign::whereUserId($shop->id)->whereNotNull('discount_id')->whereHas('has_many_campaign_customer', function($q) use($customer) {
            $q->whereCustomerId($customer->id)->whereIsUsed(0);
        })->get(['discount_id'])->pluck('discount_id')->toArray();

        $ids = $assignedLevel;
        $ids = array_merge($ids, $campaigns);
        $discounts = DiscountCode::whereIn('id',$ids)->whereUserId($shop->id)->get();

        $customer_discount = null;
        if($customer->discount_id){
            $customer_discount = $discounts->pluck('summary','id')->toArray()[$customer->discount_id];
        }


        $page = 'my-discount';
        return view("pages.widget.index",compact('settings', 'shop', 'is_login','forJoining','customer_id','customer','current_level','page','discounts', 'customer_discount'));
    }
}
