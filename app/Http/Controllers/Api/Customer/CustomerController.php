<?php

namespace App\Http\Controllers\Api\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function saveAppUser(Request $request)
    {
        $shop = User::whereUrl($request->shop)->first();
        if(empty($shop) || empty($request->customer) || empty($request->unique_id)){
            $log = "AUTHORIZATION FAILED!";
            \Log::info($log);
            return $log;
        }

        try {
            $cus = $shop->api()->rest('GET', 'admin/customers/'.$request->customer.'.json');
            $cus = $cus->body();
        } catch (\Exception $e) {
            \Log::info("SOMETHING WENT WRONG, TRY AGAIN.");
            return [$e->getMessage(), $e->getLine()];
        }

        $result = $this->postDataToSL([
            "params" => array(
                'shopifyId' => (string)$cus['id'],
                'firstName' => $cus['first_name'],
                'lastName' => $cus['last_name'],
                'email' => $cus['email'],
                'uniqueId' => $request->unique_id
            ),
            "method"=> "AddUserAcc",
        ]);
        if(!empty($result['result']['barcode']))
        {
            $cus['barcode'] = $result['result']['barcode'];
            $isExist = Customer::whereCustomerId((string)$cus['id'])->first();
            if(empty($isExist))
            {
                return $this->join($request, $cus);
            }
            else
            {
                $isExist->barcode = $result['result']['barcode'];
                $isExist->save();
            }
        }
        return '';
    }
}
