<?php

namespace App\Http\Controllers;

use App\Jobs\ExecuteWebhookJob;
use App\Models\AssignedCustomerLevel;
use App\Models\Campaign;
use App\Models\CampaignCustomer;
use App\Models\Customer;
use App\Models\CustomerLevel;
use App\Models\DiscountCode;
use App\Models\EarningPoint;
use App\Models\Webhook;
use App\Notifications\UserBirthdayNotification;
use App\User;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function index() {

        $user = new User();
        $user->email = "info.vishalgohil@gmail.com";
        $user->name = "Veed";
        $extra['points'] = 1020;
        $extra['store_name'] = "ASWSASD";
        $user->notify( new UserBirthdayNotification($extra, $user));
        //\Notification::send($user, new UserBirthdayNotification($extra, $user));
        dd("asdas");




        $shop = \Auth::user();
        $store = $shop->api()->rest('GET', 'admin/shop.json');
        $shop->store_name = $store->body->shop->name;
        $shop->save();

        $webhook = Webhook::where('is_executed',1)->find(2);

        if($webhook->topic == "orders/create"){
            $this->orderCreate($webhook);
        }
    }
    public function orderCreate($webhook){

        $data = json_decode($webhook->data,1);
        $shop = User::find($webhook->user_id);
        $customer = Customer::where('user_id', $webhook->user_id)->where('customer_id',$data['customer']['id'])->first();
        if(!$customer){
            $webhook->delete();
            return true;
        }
        $discount = array_column($data['discount_codes'],'code');
        $discount_codes = DiscountCode::where('discount_code', $discount[0])->first();

        if($discount_codes) {
            $add_points = 0;
            $campaign_customers = $discount_codes->has_one_campaign()->first()->has_many_campaign_customer()->where('is_used',0)->where('user_id', $webhook->user_id)->where('customer_id',$customer->id)->first();
            if($campaign_customers)
            {
                $campaign_customers->is_used = 1;
                $campaign_customers->save();

                $webhook->is_executed = 1;
                $webhook->save();

                $customer->spent = $customer->spent + $data['total_price'];
                $customer->save();
                $add_points = 1;
            }else{
                $assigned_customer_levels = $discount_codes->has_one_customer_level()->first()->has_many_assigned_customer()->where('user_id', $webhook->user_id)->where('customer_id',$customer->id)->first();
                if($assigned_customer_levels){
                    $webhook->is_executed = 1;
                    $webhook->save();

                    $customer->spent = $customer->spent + $data['total_price'];
                    $customer->save();
                    $add_points = 1;
                }
            }
            $earningPoint = EarningPoint::whereStatus(1)->whereUserId($webhook->user_id)->whereSlug('shopping')->first();
            if($earningPoint && $data['total_price'] >= $earningPoint->amount && $add_points){
                $points = $data['total_price'] / $earningPoint->amount;

                $customer_ep = new \App\Models\CustomerEarningPoint;
                $customer_ep->user_id = $shop->id;
                $customer_ep->earning_point_id = $earningPoint->id;
                $customer_ep->customer_id = $customer->id;
                $customer_ep->points = $points;
                $customer_ep->save();

                $customer->points = $customer->points + $points;
                $customer->balance = $customer->balance + $points;
                $customer->save();
            }

        }
    }
}
