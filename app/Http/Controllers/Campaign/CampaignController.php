<?php

namespace App\Http\Controllers\Campaign;

use App\Http\Controllers\Controller;
use App\Http\Requests\CampaignRequest;
use App\Models\Campaign;
use App\Models\CustomerLevel;
use App\Models\DiscountCode;
use App\Models\EarningPoint;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CampaignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->filled('api'))
            return $this->apiIndex($request);
        return view('pages.campaign.index');
    }

    public function apiIndex($request){
        if($request->filled('type')) {
            return $this->{$request->type}($request);
        }

        $per_page = ($request->filled('per_page'))?$request->per_page:15;
        $campaign = Campaign::with('belongs_to_customer_level','belongs_to_discount')->whereUserId(getShop()->id)->orderBy('label','asc')->paginate($per_page);
        return response($campaign,Response::HTTP_OK);
    }

    public function customerLevel($request){
        $response = CustomerLevel::whereUserId(getShop()->id)->select(['id','label'])->orderBy('label','asc')->get();
        return response($response,Response::HTTP_OK);
    }

    public function earningPoints($request) {
        $response = EarningPoint::whereUserId(getShop()->id)->whereStatus(1)->whereNotIn('slug',['sign-up'])->select(['id','label', 'points'])->orderBy('label','asc')->get();
        return response($response,Response::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CampaignRequest $request)
    {
        $campaign = new Campaign;
        $campaign->user_id = getShop()->id;
        $campaign->type = $request->type;
        $campaign = $this->common($request, $campaign);
        $campaign->save();
        return response(['message' => "Campaign successfully created."], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Campaign $campaign)
    {
        $campaign->start_at = Carbon::parse($campaign->start_at)->format('Y-m-d');
        if($campaign->end_at)
            $campaign->end_at = Carbon::parse($campaign->end_at)->format('Y-m-d');
        return response($campaign   ,200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CampaignRequest $request, Campaign $campaign)
    {
        $campaign = $this->common($request, $campaign);
        $campaign->save();
        return response(['message' => "Campaign successfully updated."], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function common($request, $campaign) {
        $campaign->status = $request->status;
        $campaign->label = $request->label;
        $campaign->start_at = @$request->start_at;
        $campaign->end_at = @$request->end_at;
        $campaign->customer_level_id = $request->customer_level_id;
        $campaign->customer_age_from = $request->customer_age_from;
        $campaign->customer_age_to = $request->customer_age_to;
        $campaign->customer_gender = $request->customer_gender;
        $campaign->earning_point_id = $request->earning_point_id;
        $campaign->require_points = $request->require_points;

        if($request->discount_id && $campaign->id == ""){
            $campaign->discount_id = $request->discount_id;
            $discount = DiscountCode::find($request->discount_id);
            $discount->used_on = $request->type;
            $discount->save();
        }

        return $campaign;
    }
}
