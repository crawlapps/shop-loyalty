<?php

namespace App\Http\Controllers\Customers;

use App\Http\Controllers\Controller;
use App\Http\Requests\CustomerRequest;
use App\Models\Customer;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CustomersController extends Controller
{

    public function __construct()
    {
        $this->middleware('check.plan.features', ['only' => [ 'store']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->filled('api'))
            return $this->apiIndex($request);
        return view('pages.customers.index');
    }

    public function apiIndex($request){
        $per_page = ($request->filled('per_page'))?$request->per_page:15;
        $customers = Customer::whereUserId(getShop()->id)->orderBy('name','asc');


        if($request->filled('recent'))
            $customers = $customers->whereRaw('DATE(created_at) = DATE_SUB(CURDATE(), INTERVAL 7 DAY)');

        $customers = $customers->paginate($per_page);
        return response($customers,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CustomerRequest $request)
    {
        $customer = new Customer();
        $customer->user_id = getShop()->id;
        $customer->name = $request->name;
        $customer->email = $request->email;
        $customer->dob = Carbon::parse($request->dob)->format('Y-m-d');
        $customer->gender = $request->gender;
        $customer->points = $request->points;
        $customer->start_date = Carbon::now();
        $customer->save();

        return response(['message' => "Customer successfully created."], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        return response($customer,200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CustomerRequest $request, Customer $customer)
    {
        $customer->name = $request->name;
        $customer->email = $request->email;
        $customer->dob = Carbon::parse($request->dob)->format('Y-m-d');
        $customer->gender = $request->gender;
        $customer->points = $request->points;
        $customer->save();

        return response(['message' => "Customer successfully updated."], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
