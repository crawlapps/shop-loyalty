<?php

namespace App\Http\Controllers\DisplaySetting;

use App\Http\Controllers\Controller;
use App\Models\DisplaySetting;
use App\Traits\ImageTrait;
use http\Client\Response;
use Illuminate\Http\Request;

class DisplaySettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.display-setting.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $shop = getShop();
        $displaySetting = DisplaySetting::firstOrNew(['user_id' => $shop->id]);

        if ($request->hasFile('logo_path')) {
            $path = "/widget/";
            if (@$displaySetting->id && @$displaySetting->logo_path) {
                $logo = $path . $displaySetting->logo_path;
                ImageTrait::deleteImage($logo);
            }
            //$options = ['width' => 150, 'height' => 93];
            $file = ImageTrait::makeImage($request->logo_path, $path);
            $displaySetting->logo_path = $file;
        }
        $displaySetting->common_data = json_decode($request->common_data,1);
        $displaySetting->widget_text = $request->widget_text?$request->widget_text:null;
        $displaySetting->save();

        return response(["message" => "Successfully Saved."], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $displaySetting = DisplaySetting::where('user_id',$id)->first();
        return response($displaySetting,200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
