<?php

namespace App\Http\Controllers\EarningPoints;

use App\Http\Controllers\Controller;
use App\Models\EarningPoint;
use Illuminate\Http\Request;

class EarningPointsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->filled('api'))
           return $this->apiIndex($request);
        return view('pages.earningPoints.index');
    }

    public function apiIndex($request) {
        if($request->filled('methodType')){
            $methodType=$request->methodType;
            return $this->$methodType($request);
        }

        $shop = getShop();
        $earningPoints = EarningPoint::whereUserId($shop->id)->get();
        return response($earningPoints,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $earningPoints = EarningPoint::find($id);
        return response($earningPoints,200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $earningPoints = EarningPoint::find($id);
        $earningPoints->points = $request->points;
        if($earningPoints->slug == "shopping")
            $earningPoints->amount = $request->amount;
        $earningPoints->save();

        return response(['message' => "Successfully updated."],200);
    }

    public function statusChanged($request){

        $status = $request->status == "true" ? 1 : 0;

        if($status == 0){
            $earning = EarningPoint::whereUserId(\Auth::user()->id)->whereStatus(1)->count();
            if($earning <= 1)
                return response(['message' => "At least one rule should be enable."],422);
        }

        $earningPoints = EarningPoint::find($request->id);
        $earningPoints->status = $status;
        $earningPoints->save();

        return response(['message' => "Successfully updated."],200);
    }
}
