<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Jobs\SyncDiscountJob;
use App\Models\Campaign;
use App\Models\CampaignCustomer;
use App\Models\Customer;
use App\Models\EarningPoint;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(Request $request)
    {

        if($request->filled('api')){
            return $this->apiIndex($request);
        }
        $earning = EarningPoint::whereUserId(\Auth::user()->id)->whereStatus(1)->count();
        if(!$earning)
            return view('pages.earningPoints.index');

        return view('pages.dashboard.index');
    }

    public function apiIndex($request) {
        $shop = \Auth::user();
        $response['campaign'] = Campaign::whereUserId($shop->id)->whereStatus(1)->count();
        $response['customers'] = Customer::whereUserId($shop->id)->count();
        $response['availed_offer'] = CampaignCustomer::whereIsUsed(1)->whereUserId($shop->id)->groupBy('customer_id')->count();

        return response($response,200);
    }
}
