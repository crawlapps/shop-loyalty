<?php

namespace App\Http\Controllers\ChoosePlan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Osiset\ShopifyApp\Storage\Models\Charge;

class ChoosePlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('pages.choose_plan');
    }

    public function chooseFreePlan(){

        $shop  = \Auth::user();

        $charge = Charge::where('user_id',$shop->id)->where('status',"ACTIVE")->first();
        if($charge){
            $response = $shop->api()->rest("DELETE",'/admin/api/recurring_application_charges/'.$charge->charge_id);
            if($response->status == 200){
                $response = $shop->api()->rest("GET",'/admin/api/recurring_application_charges/'.$charge->charge_id);
                $response = $response->bodyArray['recurring_application_charge'];
                $new_charge = $charge;
                //$new_charge->charge_id = $response['id'];
                //$new_charge->user_id = $shop->id;
                /*$new_charge->plan_id = @$charge->plan_id;
                $new_charge->type = $charge->type;*/
                $new_charge->status = $response['status'];
                $new_charge->name = $response['name'];
                //$new_charge->price = $response['price'];
                //$new_charge->test = $response['test'];
                //$new_charge->trial_days = $response['trial_days'];
                //$new_charge->trial_ends_on = $response['trial_ends_on'];
                $new_charge->cancelled_on = $response['cancelled_on'];
                $new_charge->expires_on = $response['cancelled_on'];
                //$new_charge->activated_on = $response['activated_on'];
                $new_charge->save();
            }
        }

        $shop = \Auth::user();
        $shop->plan_id = 1;
        $shop->save();

        return redirect()->route('home');
    }
}
