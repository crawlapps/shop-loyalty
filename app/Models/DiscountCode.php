<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\DiscountCode
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $discount_id
 * @property string|null $status
 * @property mixed|null $response
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCode newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCode newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCode query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCode whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCode whereDiscountId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCode whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCode whereResponse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCode whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCode whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCode whereUserId($value)
 * @mixin \Eloquent
 * @property string|null $discount_code
 * @property string|null $summary
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCode whereDiscountCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCode whereSummary($value)
 * @property string|null $applies_to
 * @property string|null $used_on
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCode whereAppliesTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCode whereUsedOn($value)
 */
class DiscountCode extends Model
{
    public $fillable = [
      'user_id',
      'discount_id'
    ];

    public $casts = [
      'response' => 'array'
    ];

    public function has_one_campaign(){
        return $this->hasOne(Campaign::class,'discount_id','id');
    }

    public function has_one_customer_level(){
        return $this->hasOne(CustomerLevel::class,'discount_id','id');
    }
}
