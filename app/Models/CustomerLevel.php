<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CustomerLevel
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $label
 * @property string|null $discount_id Feed from shopify, discount id
 * @property string|null $discount_code Feed from shopify
 * @property string|null $discount_type Feed from shopify
 * @property float $require_points
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Customer[] $belongs_to_many_customer
 * @property-read int|null $belongs_to_many_customer_count
 * @property-read \App\User $belongs_to_user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLevel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLevel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLevel query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLevel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLevel whereDiscountCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLevel whereDiscountId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLevel whereDiscountType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLevel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLevel whereLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLevel whereRequirePoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLevel whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLevel whereUserId($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\AssignedCustomerLevel[] $has_many_assigned_customer
 * @property-read int|null $has_many_assigned_customer_count
 * @property-read \App\Models\DiscountCode $belongs_to_discount
 */
class CustomerLevel extends Model
{
    public function belongs_to_user(){
        return $this->belongsTo(User::class);
    }

    public function belongs_to_many_customer(){
        return $this->belongsToMany(Customer::class);
    }

    public function belongs_to_discount(){
        return $this->belongsTo(DiscountCode::class,'discount_id','id');
    }

    public function has_many_assigned_customer(){
        return $this->hasMany(AssignedCustomerLevel::class,'customer_level_id','id');
    }
}
