<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Customer
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $name
 * @property string|null $email
 * @property string|null $dob
 * @property string|null $gender
 * @property float $points
 * @property float $spent
 * @property float $balance
 * @property string|null $loyalty_number
 * @property string|null $card_barcode
 * @property string|null $card_barcode_1
 * @property string|null $start_date
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereBalance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCardBarcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCardBarcode1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereDob($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereLoyaltyNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer wherePoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereSpent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereUserId($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CustomerLevel[] $belongs_to_many_level
 * @property-read int|null $belongs_to_many_level_count
 * @property string|null $customer_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCustomerId($value)
 */
class Customer extends Model
{
    public function belongs_to_many_level(){
        return $this->belongsToMany(CustomerLevel::class);
    }

    public function blg_shop(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function has_many_assigned_level(){
        return $this->hasMany(AssignedCustomerLevel::class);
    }

}
