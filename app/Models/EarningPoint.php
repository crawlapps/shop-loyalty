<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\EarningPoint
 *
 * @property int $id
 * @property int $user_id
 * @property string $label
 * @property string $slug
 * @property float $points
 * @property int $status 1: Enable; 0: Disable
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\User $belongs_to_user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EarningPoint newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EarningPoint newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EarningPoint query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EarningPoint whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EarningPoint whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EarningPoint whereLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EarningPoint wherePoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EarningPoint whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EarningPoint whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EarningPoint whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EarningPoint whereUserId($value)
 * @mixin \Eloquent
 */
class EarningPoint extends Model
{
    public $fillable = [
        'user_id',
        'label',
        'slug',
        'points',
        'status'
    ];

    public function belongs_to_user(){
        return $this->belongsTo(User::class,'user_id','id');
    }
}
