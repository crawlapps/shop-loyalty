<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\OfferProduct
 *
 * @property int $id
 * @property int $user_id
 * @property int $campaign_id
 * @property string|null $product_id shopify product id
 * @property string|null $product_title shopify product title
 * @property string|null $product_image_url shopify product image url
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OfferProduct newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OfferProduct newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OfferProduct query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OfferProduct whereCampaignId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OfferProduct whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OfferProduct whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OfferProduct whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OfferProduct whereProductImageUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OfferProduct whereProductTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OfferProduct whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OfferProduct whereUserId($value)
 * @mixin \Eloquent
 */
class OfferProduct extends Model
{
    //
}
