<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CustomerEarningPoint
 *
 * @property int $id
 * @property int $user_id
 * @property int $earning_point_id
 * @property float $points
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerEarningPoint newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerEarningPoint newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerEarningPoint query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerEarningPoint whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerEarningPoint whereEarningPointId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerEarningPoint whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerEarningPoint wherePoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerEarningPoint whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerEarningPoint whereUserId($value)
 * @mixin \Eloquent
 * @property int $customer_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerEarningPoint whereCustomerId($value)
 */
class CustomerEarningPoint extends Model
{
    //
}
