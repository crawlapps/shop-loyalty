<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Campaign
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $type campaign, offer
 * @property int $status 1:enable, 0:disable
 * @property string|null $label
 * @property string|null $start_at
 * @property string|null $end_at
 * @property int $customer_level_id
 * @property int|null $customer_age_from
 * @property int|null $customer_age_to
 * @property string|null $customer_gender male, female, other
 * @property int $earning_point_id
 * @property float $require_points
 * @property int $discount_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Campaign newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Campaign newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Campaign query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Campaign whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Campaign whereCustomerAgeFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Campaign whereCustomerAgeTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Campaign whereCustomerGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Campaign whereCustomerLevelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Campaign whereDiscountId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Campaign whereEarningPointId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Campaign whereEndAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Campaign whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Campaign whereLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Campaign whereRequirePoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Campaign whereStartAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Campaign whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Campaign whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Campaign whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Campaign whereUserId($value)
 * @mixin \Eloquent
 * @property-read \App\Models\CustomerLevel $belongs_to_customer_level
 * @property-read \App\Models\DiscountCode|null $belongs_to_discount
 */
class Campaign extends Model
{

    protected $casts = [
        'status' => 'boolean',
    ];

    public function belongs_to_discount(){
        return $this->belongsTo(DiscountCode::class, 'discount_id', 'id');
    }

    public function belongs_to_customer_level(){
        return $this->belongsTo(CustomerLevel::class, 'customer_level_id', 'id');
    }

    public function belongs_to_earning_point(){
        return $this->belongsTo(EarningPoint::class, 'earning_point_id', 'id');
    }

    public function has_many_campaign_customer(){
        return $this->hasMany(CampaignCustomer::class);
    }
}
