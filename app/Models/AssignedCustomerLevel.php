<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\AssignedCustomerLevel
 *
 * @property int $id
 * @property int $user_id
 * @property int $customer_level_id
 * @property int $customer_id
 * @property string|null $discount_id
 * @property string|null $discount_code
 * @property int $status 0: Deactive; 1: Active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AssignedCustomerLevel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AssignedCustomerLevel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AssignedCustomerLevel query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AssignedCustomerLevel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AssignedCustomerLevel whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AssignedCustomerLevel whereCustomerLevelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AssignedCustomerLevel whereDiscountCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AssignedCustomerLevel whereDiscountId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AssignedCustomerLevel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AssignedCustomerLevel whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AssignedCustomerLevel whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AssignedCustomerLevel whereUserId($value)
 * @mixin \Eloquent
 */
class AssignedCustomerLevel extends Model
{
    protected $fillable = ['user_id', 'customer_id', 'customer_level_id', 'status'];

    public function blg_customer_level(){
        return $this->belongsTo(CustomerLevel::class,'customer_level_id','id');
    }
}
