<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\DisplaySetting
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $widget_text
 * @property string|null $logo_path
 * @property mixed|null $common_data
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DisplaySetting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DisplaySetting newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DisplaySetting query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DisplaySetting whereCommonData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DisplaySetting whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DisplaySetting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DisplaySetting whereLogoPath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DisplaySetting whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DisplaySetting whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DisplaySetting whereWidgetText($value)
 * @mixin \Eloquent
 */
class DisplaySetting extends Model
{
    public $fillable = [
      'user_id',
    ];

    public $casts = [
        'common_data' => 'array'
    ];
}
