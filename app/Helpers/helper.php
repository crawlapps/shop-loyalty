<?php
function shopifyPlans(){
    return [
        'loyalty_starter' =>[
            'id' => 1,
            'price' => 0,
            'title' => 'Loyalty Starter',
        ],
        'online_loyalty' =>[
            'id' => 2,
            'price' => 49,
            'title' => 'Online Loyalty',
        ],
        'online_offline' =>[
            'id' => 3,
            'price' => 199,
            'title' => 'Online and Offline',
        ],
        'enterprise' =>[
            'id' => 4,
            'price' => 499,
            'title' => 'Enterprise',
        ]
    ];
}

function getShop(){
    return \Auth::user();
}

function getDiscountCodes($request) {
    $discount =  \App\Models\DiscountCode::whereUserId(getShop()->id)->select(['id','summary','discount_code', 'applies_to']);

    $discount = $discount->when($request->mode == 'new', function($q){
        $q->whereNull('used_on');
    });

    $discount = $discount->when(@$request->filter, function($q) use ($request){
        if($request->filter=="offer")
            $q->where('applies_to', 'products');

        if($request->filter=="campaign")
            $q->whereNull('applies_to');
    });

    return $discount->get();
}

function storeCustomerEarningPoints(\App\Models\Customer $customer, \App\Models\EarningPoint $earningPoint, \App\User $shop){
    $customer_ep = new \App\Models\CustomerEarningPoint;
    $customer_ep->user_id = $shop->id;
    $customer_ep->earning_point_id = $earningPoint->id;
    $customer_ep->customer_id = $customer->id;
    $customer_ep->points = $earningPoint->points;
    $customer_ep->save();

    $customer->points = $customer->points + $earningPoint->points;
    $customer->balance = $customer->balance + $earningPoint->points;
    $customer->save();
    return true;
}

function checkPlanFeatures($shop = null ){
    $features = [
        'customers' => true,
        'logo' => true,
    ];
    if(!$shop)
        $shop = getShop();

    $customers = \App\Models\Customer::whereUserId($shop->id)->count();
    if($shop->plan_id === 1){
        $features = [
            'customers' => ($customers >= 100)?false:true,
            'logo' => false,
        ];
    }else if($shop->plan_id === 2){
        $features = [
            'customers' => ($customers >= 10000)?false:true,
            'logo' => true,
        ];
    }
    return $features;
}
