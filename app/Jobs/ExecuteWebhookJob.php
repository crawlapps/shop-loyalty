<?php

namespace App\Jobs;

use App\Models\Customer;
use App\Models\CustomerEarningPoint;
use App\Models\DiscountCode;
use App\Models\EarningPoint;
use App\Models\Webhook;
use App\Notifications\UserShoppingNotification;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ExecuteWebhookJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $webhook_id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($webhook_id)
    {
        $this->webhook_id = $webhook_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $webhook = Webhook::where('is_executed',1)->find($this->webhook_id);
        if($webhook) {
            if ($webhook->topic == "orders/create") {
                $this->orderCreate($webhook);
            }
        }
    }

    public function orderCreate($webhook){

        $data = json_decode($webhook->data,1);
        $shop = User::find($webhook->user_id);
        $customer = Customer::where('user_id', $webhook->user_id)->where('customer_id',$data['customer']['id'])->first();
        if(!$customer){
            $webhook->delete();
            return true;
        }
        $discount = array_column($data['discount_codes'],'code');
        $discount_codes = DiscountCode::where('discount_code', $discount[0])->first();

        if($discount_codes) {
            $add_points = 0;
            $campaign_customers = $discount_codes->has_one_campaign()->first()->has_many_campaign_customer()->where('is_used',0)->where('user_id', $webhook->user_id)->where('customer_id',$customer->id)->first();
            if($campaign_customers)
            {
                $campaign_customers->is_used = 1;
                $campaign_customers->save();

                $webhook->is_executed = 1;
                $webhook->save();

                $customer->spent = $customer->spent + $data['total_price'];
                $customer->save();
                $add_points = 1;
            }else{
                $assigned_customer_levels = $discount_codes->has_one_customer_level()->first()->has_many_assigned_customer()->where('user_id', $webhook->user_id)->where('customer_id',$customer->id)->first();
                if($assigned_customer_levels){
                    $webhook->is_executed = 1;
                    $webhook->save();

                    $customer->spent = $customer->spent + $data['total_price'];
                    $customer->save();
                    $add_points = 1;
                }
            }
            $earningPoint = EarningPoint::whereStatus(1)->whereUserId($webhook->user_id)->whereSlug('shopping')->first();
            if($earningPoint && $data['total_price'] >= $earningPoint->amount && $add_points){
                $points = $data['total_price'] / $earningPoint->amount;

                $customer_ep = new \App\Models\CustomerEarningPoint;
                $customer_ep->user_id = $shop->id;
                $customer_ep->earning_point_id = $earningPoint->id;
                $customer_ep->customer_id = $customer->id;
                $customer_ep->points = $points;
                $customer_ep->save();

                $customer->points = $customer->points + $points;
                $customer->balance = $customer->balance + $points;
                $customer->save();

                $customer_ep = CustomerEarningPoint::whereCustomerId($customer->id)
                    ->whereUserId($shop->id)
                    ->whereEarningPointId($earningPoint->id);
                if($customer_ep->get()->count() <= 1) {
                    $extra = [];
                    $user = new User();
                    $user->email = $customer->email;
                    $user->name = $customer->name;
                    $extra['points'] = $points;
                    $extra['currency'] = $shop->currency;
                    $extra['store_name'] = $shop->store_name;
                    \Notification::send($user, new UserShoppingNotification($extra, $user));
                }

            }

        }
    }
}
