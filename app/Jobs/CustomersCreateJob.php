<?php namespace App\Jobs;

use App\Models\Customer;
use App\Models\EarningPoint;
use App\Notifications\UserSignupNotification;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Osiset\ShopifyApp\Contracts\Objects\Values\ShopDomain;
use stdClass;
use Illuminate\Support\Str;

class CustomersCreateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Shop's myshopify domain
     *
     * @var ShopDomain
     */
    public $shopDomain;

    /**
     * The webhook data
     *
     * @var object
     */
    public $data;

    /**
     * Create a new job instance.
     *
     * @param string   $shopDomain The shop's myshopify domain
     * @param stdClass $data    The webhook data (JSON decoded)
     *
     * @return void
     */
    public function __construct($shopDomain, $data)
    {
        $this->shopDomain = $shopDomain;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $shopDomain = $this->shopDomain->toNative();
        $data = json_decode(json_encode($this->data),1);


        if(Str::contains($data['tags'], 'shopLoyal-customer')) {
            $shop = User::whereName($shopDomain)->first();
            if(!checkPlanFeatures($shop)['customers']){
                return true;
            }

            $customer = Customer::where('user_id', $shop->id)->where('customer_id', $data['id'])->first();
            if($customer)
                return true;

            $signup_points = EarningPoint::whereStatus(1)->whereSlug("sign-up")->whereUserId($shop->id)->first();

            if($signup_points)
                $signup_points = $signup_points->points;
            else
                $signup_points = 0;

            $barcode = "098".$shop->id."058".$data['id'];

            $customer = new Customer;
            $customer->user_id = $shop->id;
            $customer->customer_id = $data['id'];
            $customer->name = $data['first_name']." ".$data['last_name'];
            $customer->email = $data['email'];
            //$customer->points = $signup_points;
            //$customer->balance = $signup_points;
            $customer->loyalty_number = $barcode;
            $customer->start_date = date("Y-m-d H:i:s");
            $customer->save();

            if($signup_points) {
                storeCustomerEarningPoints($customer, $signup_points, $shop);
                $extra = [];
                $user = new User();
                $user->email = $customer->email;
                $user->name = $customer->name;
                $extra['store_name'] = $shop->store_name;
                \Notification::send($user, new UserSignupNotification($extra, $user));
            }

        }
        return true;
    }
}
