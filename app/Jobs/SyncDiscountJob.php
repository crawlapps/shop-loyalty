<?php

namespace App\Jobs;

use App\Models\DiscountCode;
use App\Traits\GraphQLTrait;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SyncDiscountJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use GraphQLTrait;

    public $shop = null;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($shop_id)
    {
        $this->shop = $shop_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->shop = User::find($this->shop);
        $this->recursive($this->shop, 'priceRules', $limit = 50, $after = null, $existData = [], $call = 0, 'priceRulesStoreInDb', 'priceRules');
    }

    public function priceRules(User $shop, $scope, $limit = 250, $after = null){
        $after = (!empty($after))? " after: ".$after:"";
        $query = 'query MyQuery {
                      ' . $scope . '(first: '.$limit.$after.' query: "status:Active") {
                        edges {
                              node {
                                discountCodes(first: 10) {
                                  edges {
                                    node {
                                      id
                                      code
                                    }
                                  }
                                }
                                itemEntitlements {
                                  products(first: 1) {
                                    edges {
                                      node {
                                        id
                                      }
                                    }
                                  }
                                }
                                legacyResourceId
                                traits
                                summary
                                customerSelection {
                                  forAllCustomers
                                }
                              }
                            }
                          }
                    }';
        $parameters = ['limit' => $limit];
        if ($after) {
            $parameters['after'] = $after;
        }
        $data = $this->graph($shop, $query, $parameters);
        return $data;
    }

    public function recursive(User $shop, $scope, $limit = 250, $after = null, $existData = [], $call = 0, $functionName = null, $queryFunction = null)
    {
        $data = $this->$queryFunction($shop, $scope, $limit, $after);
        $checkNext = $data->body->$scope->pageInfo->hasNextPage ?? false;
        if (!isset($data->body->$scope) && isset($data->errors) && $data->errors == true) {
            // Just in case if body returns null instead of data
            \Log::info(json_encode($data));
            throw new \Exception($data->body);
        }
        if (isset($data->body->$scope->edges)) {
            $after = end($data->body->$scope->edges)->cursor ?? null;
            $responseData = [];
            foreach ($data->body->$scope->edges as $node) {

                $responseData[] = $node;
            }
            $this->$functionName($shop, $responseData);
            //$existData = array_merge($existData, $responseData);
        }
        if ($checkNext) {
            $call++;
            return $this->recursive($shop, $scope, $limit, $after, $existData, $call, $functionName,$queryFunction);
        }
        \Log::info("-----------------END--------------------");
        return true;
    }

    public function priceRulesStoreInDb(User $shop, $responseData) {
        //dd($responseData);
        foreach ($responseData as $key => $val) {
            $val = $val->node;

            if(in_array("BUY_ONE_GET_ONE", $val->traits) || in_array("BUY_ONE_GET_ONE_WITH_ALLOCATION_LIMIT", $val->traits) ){
                continue;
            }
            if(in_array("SPECIFIC_CUSTOMERS", $val->traits)){

                $discount = $val->discountCodes->edges[0]->node;
                $discount_id = str_replace("gid://shopify/PriceRuleDiscountCode/","", $discount->id );
                try{

                    $response['traits'] = $val->traits;
                    $response['price_rule_id'] = $val->legacyResourceId;
                    $response['summary'] = $val->summary;
                    $response['customerSelection'] = $val->customerSelection->forAllCustomers;
                    $response['discount_id'] = $discount_id;
                    $response['discount_code'] = $discount->code;
                    \DB::beginTransaction();
                    $discount = DiscountCode::firstOrNew(['user_id' => $shop->id, 'discount_id' =>  $discount_id]);
                    $discount->discount_code = $response['discount_code'];
                    $discount->summary = $val->summary;
                    $discount->response = $response;
                    $discount->applies_to = count($val->itemEntitlements->products->edges) ? "products" : Null;
                    $discount->save();
                    \DB::commit();
                }catch (\Exception $e){
                    \DB::rollback();
                }
            }
        }
    }

}
