<?php

namespace App\Jobs;

use App\Models\DisplaySetting;
use App\Models\EarningPoint;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AfterAuthenticateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $shop = \Auth::user();

        $setting["btn_bg_color"] = "#ff9600";
        $setting["btn_text_color"] = "#FFFFFF";
        $setting["btn_border_size"] = "1";
        $setting["btn_border_color"] = "#ff9600";
        $setting["section_bg_color"] = "#740E0E";
        $setting["btn_border_radius"] = "1";
        $setting["section_text_color"] = "#FFFFFF";
        $setting["section_header_text"] = "Loyalty Program";
        $setting["section_header_color"] = "#FFFFFF";
        $setting["section_header_bg_color"] = "#8404A6";


        $displaySetting = DisplaySetting::whereUserId($shop->id)->first();
        if(!$displaySetting){
            $displaySetting = new DisplaySetting;
            $displaySetting->user_id = $shop->id;
            $displaySetting->common_data = $setting;
            $displaySetting->save();
        }


        $earning = $shop->has_many_earning()->count();
        if(!$earning){




            $earningPoints = [
                [
                    "user_id" => $shop->id,
                    "label" => "Birthday",
                    "slug" => "birthday",
                    "points" => 100,
                    "status" => 0
                ],
                [
                    "user_id" => $shop->id,
                    "label" => "Sign Up",
                    "slug" => "sign-up",
                    "points" => 150,
                    "status" => 0
                ],
                [
                    "user_id" => $shop->id,
                    "label" => "Visits",
                    "slug" => "visits",
                    "points" => 10,
                    "status" => 0
                ],
                [
                    "user_id" => $shop->id,
                    "label" => "New Level",
                    "slug" => "new-level",
                    "points" => 100,
                    "status" => 0
                ],
                [
                    "user_id" => $shop->id,
                    "label" => "Shopping",
                    "slug" => "shopping",
                    "points" => 1,
                    "status" => 0
                ],
            ];

            EarningPoint::insert($earningPoints);
            $this->snippet();
        }
    }

    public function snippet(){

        $shop = \Auth::user();
        $store = $shop->api()->rest('GET', 'admin/shop.json');
        $shop->store_name = $store->body->shop->name;
        $shop->currency = $store->body->shop->currency;
        $shop->save();

        $value = <<<EOF
        <script id="crawlapps_shoployal_data" type="application/json">
            {
                "shop": {
                    "domain": "{{ shop.domain }}",
                    "permanent_domain": "{{ shop.permanent_domain }}",
                    "url": "{{ shop.url }}",
                    "secure_url": "{{ shop.secure_url }}",
                    "money_format": {{ shop.money_format | json }},
                    "currency": {{ shop.currency | json }}
                },
                "customer": {
                    "id": {{ customer.id | json }},
                    "tags": {{ customer.tags | json }}
                },
                "cart": {{ cart | json }},
                "template": "{{ template | split: "." | first }}",
                "product": {{ product | json }},
                "collection": {{ collection.products | json }}
            }
        </script>
EOF;
        $sh_theme = $shop->api()->rest('GET', 'admin/themes.json',['role' => 'main']);
        $main_theme = $sh_theme->body->themes[0]->id;

        $parameter['asset']['key'] = 'snippets/crawlapps-shoployal.liquid';
        $parameter['asset']['value'] = $value;
        $asset = $shop->api()->rest('PUT', 'admin/themes/'.$main_theme.'/assets.json',$parameter);


        $asset = $shop->api()->rest('GET', 'admin/themes/'.$main_theme.'/assets.json',["asset[key]" => 'layout/theme.liquid']);
        if(@$asset->body->asset) {
            $asset = $asset->body->asset->value;
            if(!strpos($asset ,"{% include 'crawlapps-userguide' %}</head>")) {
                $asset = str_replace('</head>',"{% include 'crawlapps-shoployal' %}</head> ",$asset);
            }

            $parameter['asset']['key'] = 'layout/theme.liquid';
            $parameter['asset']['value'] = $asset;
            $asset = $shop->api()->rest('PUT', 'admin/themes/'.$main_theme.'/assets.json',$parameter);
        }
    }
}
