<?php namespace App\Jobs;

use App\Models\Webhook;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Osiset\ShopifyApp\Contracts\Objects\Values\ShopDomain;
use stdClass;

class OrdersCreateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Shop's myshopify domain
     *
     * @var ShopDomain
     */
    public $shopDomain;

    /**
     * The webhook data
     *
     * @var object
     */
    public $data;

    /**
     * Create a new job instance.
     *
     * @param string   $shopDomain The shop's myshopify domain
     * @param stdClass $data    The webhook data (JSON decoded)
     *
     * @return void
     */
    public function __construct($shopDomain, $data)
    {
        $this->shopDomain = $shopDomain;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Do what you wish with the data
        // Access domain name as $this->shopDomain->toNative()
        \Log::Info("=============== Order Create Webhook ==================");
        $shop = User::where('name',$this->shopDomain->toNative())->first();
        $order = json_encode($this->data);
        $shopify_id = json_decode($order)->id;

        $entity = Webhook::updateOrCreate(
            ['shopify_id' => $shopify_id, 'topic' => 'orders/create', 'user_id' => $shop->id],
            ['shopify_id' => $shopify_id, 'topic' => 'orders/create', 'shop_id' => $shop->id, 'data' => $order]
        );

        ExecuteWebhookJob::dispatch($entity->id);
        return true;
    }
}
