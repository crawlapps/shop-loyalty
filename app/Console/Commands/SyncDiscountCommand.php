<?php

namespace App\Console\Commands;

use App\Jobs\SyncDiscountJob;
use App\User;
use Illuminate\Console\Command;

class SyncDiscountCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shopify:sync-discount';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync discount code';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $shops = User::get();
        foreach($shops as $key => $shop){
            dispatch(new SyncDiscountJob($shop->id));
        }

    }
}
