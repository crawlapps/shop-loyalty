<?php

namespace App\Console\Commands;

use App\Models\Customer;
use App\Models\EarningPoint;
use App\Notifications\UserBirthdayNotification;
use App\User;
use Illuminate\Console\Command;

class BirthdayPointCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'birthday:point';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Give points on birthday';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $earningPoints = EarningPoint::whereStatus(1)->whereSlug('birthday')->get(['id','user_id', 'points']);
        foreach($earningPoints as $key => $val){
            $shop = User::find($val->user_id);
            $customers = Customer::whereMonth('dob','=', date('m'))->whereDay('dob','=', date('d'))->whereUserId($val->user_id)->get();
            foreach($customers as $key_c => $val_c){
                storeCustomerEarningPoints($val_c, $val, $shop);
                $extra = [];
                $user = new User();
                $user->email = $val_c->email;
                $user->name = $val_c->name;
                $extra['points'] = $val->points;
                $extra['store_name'] = $shop->store_name;
                \Notification::send($user, new UserBirthdayNotification($extra, $user));
            }
        }
    }
}
