<?php

namespace App\Console\Commands;

use App\Models\Campaign;
use App\Models\CampaignCustomer;
use App\Models\Customer;
use App\Notifications\CompaignNotification;
use App\User;
use Illuminate\Console\Command;

class CampaignCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shoployal:sync:campaign';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $shops = User::get();
        foreach($shops as $key => $shop){
            $campaigns = Campaign::whereStatus(1)->whereUserId($shop->id)->get();
            foreach($campaigns as $key_camp => $val_camp) {

                $flag = 0;
                if(is_null($val_camp->end_at ) && $val_camp->start_at <= \Carbon\Carbon::now()->format('Y-m-d 00:00:00'))
                {
                    $flag = 1;
                }
                else if(
                    $val_camp->start_at <= \Carbon\Carbon::now()->format('Y-m-d 00:00:00') &&
                    $val_camp->end_at >= \Carbon\Carbon::now()->format('Y-m-d 23:59:59')
                ){
                    $flag = 1;
                }
                if($flag == 0){
                    continue;
                }

                $customers = Customer::whereUserId($shop->id)
                    ->whereNotIn('id',$val_camp->has_many_campaign_customer()->toBase()->get(['customer_id']))
                    ->where('points', '>=', $val_camp->require_points)
                    ->whereHas('has_many_assigned_level', function($q) use ($val_camp) {
                        $q->whereCustomerLevelId($val_camp->customer_level_id);
                        $q->whereStatus(1);
                    });
                if($val_camp->type=="campaign") {
                    $customers = $customers->whereGender($val_camp->customer_gender)
                        ->whereBetween(\DB::raw('TIMESTAMPDIFF(YEAR,dob,CURDATE())'),[$val_camp->customer_age_from,$val_camp->customer_age_to]);
                }
                $customers = $customers->get();

                foreach($customers as $key_c => $val_c) {
                    if($val_camp->earning_point_id >= 1) { // add earning points
                        storeCustomerEarningPoints($val_c, $val_camp->belongs_to_earning_point, $shop);
                    }

                    if($val_camp->discount_id >= 1) { // add in shopify discount code
                        $price_rule_id = $val_camp->belongs_to_discount->response['price_rule_id'];
                        $price_rule = $shop->api()->rest('GET',"/admin/api/price_rules/{$price_rule_id}.json");
                        $parameter = [];
                        $parameter['price_rule']['prerequisite_customer_ids'] = $price_rule->bodyArray['price_rule']['prerequisite_customer_ids'];
                        $parameter['price_rule']['prerequisite_customer_ids'][] = $val_c->customer_id;
                        $price_rule = $shop->api()->rest('PUT',"/admin/api/price_rules/{$price_rule_id}.json",$parameter);
                    }

                    $campaignCustomer = new CampaignCustomer;
                    $campaignCustomer->user_id = $shop->id;
                    $campaignCustomer->campaign_id = $val_camp->id;
                    $campaignCustomer->customer_id = $val_c->id;
                    $campaignCustomer->save();

                    $extra = [];
                    $user = new User();
                    $user->email = $val_c->email;
                    $user->name = $val_c->name;
                    $extra['campaign_name'] = $val_camp->label;
                    $extra['store_name'] = $shop->store_name;
                    \Notification::send($user, new CompaignNotification($extra, $user));
                }
            }
        }
    }
}
