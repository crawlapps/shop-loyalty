<?php

namespace App\Console\Commands;

use App\Models\AssignedCustomerLevel;
use App\Models\Customer;
use App\Models\CustomerLevel;
use App\Models\EarningPoint;
use App\Notifications\UserReachedNewLevelNotification;
use App\User;
use Illuminate\Console\Command;

class NewLevelPointCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'newlevel:point';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Give points on birthday';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // email functionality is remain
        $shops = User::get();
        foreach($shops as $key => $shop){
            $earningPoints = EarningPoint::whereStatus(1)->whereUserId($shop->id)->whereSlug('new-level')->first();
            $customerLevel = CustomerLevel::whereUserId($shop->id)->get();
            $customers = Customer::whereUserId($shop->id)->get();
            foreach($customers as $key_c => $val_c){
                $level = null;
                foreach($customerLevel as $key_cl => $val_cl) {
                    if($val_c->points >= $val_cl->require_points){
                        $level = $val_cl;
                    }
                }
                if($level){

                    $current_level = AssignedCustomerLevel::whereUserId($shop->id)->whereCustomerId($val_c->id)->whereStatus(1)->first();
                    if($current_level){
                        $price_rule_id = $current_level->blg_customer_level->belongs_to_discount->response['price_rule_id'];
                        $price_rule = $shop->api()->rest('GET',"/admin/api/price_rules/{$price_rule_id}.json");

                        $array = $price_rule->bodyArray['price_rule']['prerequisite_customer_ids'];
                        if (($key = array_search($val_c->customer_id, $array)) !== false) {
                            unset($array[$key]);
                            $parameter = [];
                            $parameter['price_rule']['prerequisite_customer_ids'] = $array;
                            $price_rule = $shop->api()->rest('PUT',"/admin/api/price_rules/{$price_rule_id}.json",$parameter);
                        }

                    }
                    AssignedCustomerLevel::whereUserId($shop->id)->whereCustomerId($val_c->id)->update(['status' => 0]);
                    $assignLevel = AssignedCustomerLevel::updateOrCreate(
                        ['user_id' => $shop->id, 'customer_id' => $val_c->id, 'customer_level_id' => $level->id],
                        ['user_id' => $shop->id, 'customer_id' => $val_c->id, 'customer_level_id' => $level->id, 'status' => 1]
                    );
                    storeCustomerEarningPoints($val_c, $earningPoints, $shop);
                    $price_rule_id = $level->belongs_to_discount->response['price_rule_id'];
                    $price_rule = $shop->api()->rest('GET',"/admin/api/price_rules/{$price_rule_id}.json");
                    $parameter = [];
                    $parameter['price_rule']['prerequisite_customer_ids'] = $price_rule->bodyArray['price_rule']['prerequisite_customer_ids'];
                    $parameter['price_rule']['prerequisite_customer_ids'][] = $val_c->customer_id;
                    $price_rule = $shop->api()->rest('PUT',"/admin/api/price_rules/{$price_rule_id}.json",$parameter);

                    $extra = [];
                    $user = new User();
                    $user->email = $val_c->email;
                    $user->name = $val_c->name;
                    $extra['level_name'] = $level->label;
                    $extra['store_name'] = $shop->store_name;
                    \Notification::send($user, new UserReachedNewLevelNotification($extra, $user));
                }
            }
        }
    }
}
