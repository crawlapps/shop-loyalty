
<div class="app-header">
    <a class="app-brand" href="#"><b><i>Shoployal</i></b></a>
</div>

@if($earning)
    <div class="app-sidebar">
        <ul class="list-unstyled">
            <li>
                <a href="{{ route("home") }}" class="active">
                    <span><i class="material-icons app-side-icon">home</i>Home</span>
                </a>

            </li>
            <li class="submenu">
                <a href="#">
                    <span><i class="material-icons app-side-icon">people</i>Customers</span>
                </a>
                <ul class="list-unstyled">
                    <li><a href="{{ route("customers.index") }}">All Customers</a></li>
                    <li><a href="{{ route("customer-level.index") }}">Customer Levels</a></li>
                </ul>
            </li>
            <li>
                <a href="{{ route("campaign.index") }}">
                    <span><i class="material-icons app-side-icon">view_list</i>Campaigns</span>
                </a>
            </li>
            <li class="submenu">
                <a href="#">
                    <span><i class="material-icons app-side-icon">settings</i>Settings</span>
                </a>
                <ul class="list-unstyled">
                    <li><a href="{{ route("earning-points.index") }}">Reward program</a></li>
                    <li><a href="{{ route("display-setting.index") }}">Display Settings</a></li>
                </ul>
            </li>

            <li>
                <a href="{{ route("choose.plan.index") }}">
                    <span><i class="material-icons app-side-icon">settings</i>Subscription Plans</span>
                </a>
            </li>

            <li>
                <a href="{{ route("installation_guide") }}">
                    <span><i class="material-icons app-side-icon">settings</i>Installation Guide</span>
                </a>
            </li>
        </ul>
    </div>
    <script>
        $('.bars').click(function(){
            $(this).toggleClass('active');
            $('.app-sidebar').toggleClass('active');
        });

        $('.submenu > a').click(function(){
            $(this).parents('.submenu').toggleClass('active');
        });
    </script>
@endif
