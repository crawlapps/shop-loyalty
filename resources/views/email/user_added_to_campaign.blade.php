@extends('email.index')
@section('email-content')
    <tr>
        <td align='center'>
            <center>
                <table border='0' cellpadding='30' cellspacing='0'
                       style='margin-left: auto;margin-right: auto;width:600px;text-align:center;' width='600'>
                    <tr>
                        <td align='left' style='background: #ffffff; border: 1px solid #dce1e5;' valign='top' width=''>
                            <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                                <tr>
                                    <td align='center' valign='top'>
                                        <h2>
                                            Dear {!! $user->name  !!}
                                        </h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td align='center'
                                        style='border-top: 1px solid #dce1e5;border-bottom: 1px solid #dce1e5;'
                                        valign='top'>
                                        <p style='margin: 1em 0;'>
                                            Congratulations! You have been granted a special gift for your loyalty! You are selected to receive all the benefits of our {{ $extra['campaign_name'] }} campaign!
                                        </p>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left" valign='top'>
                                        <p style='margin: 1em 0;'>
                                            All the best,
                                            <br>
                                            {{ $extra['store_name'] }}
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </center>
        </td>
    </tr>
@stop
