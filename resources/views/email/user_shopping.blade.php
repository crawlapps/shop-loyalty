@extends('email.index')
@section('email-content')
    <tr>
        <td align='center'>
            <center>
                <table border='0' cellpadding='30' cellspacing='0'
                       style='margin-left: auto;margin-right: auto;width:600px;text-align:center;' width='600'>
                    <tr>
                        <td align='left' style='background: #ffffff; border: 1px solid #dce1e5;' valign='top' width=''>
                            <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                                <tr>
                                    <td align='center' valign='top'>
                                        <h2>
                                            Dear {!! $user->name  !!}
                                        </h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td align='center'
                                        style='border-top: 1px solid #dce1e5;border-bottom: 1px solid #dce1e5;'
                                        valign='top'>
                                        <p style='margin: 1em 0;'>
                                            Thank you for your purchase! We are happy to see you enjoying our store.  We appreciate loyalty, therefore every time you purchase something, you will be rewarded {{ $extra['points'] }} points for every {{ $extra['currency'] }} you spend. With more points, you can get to higher levels, and enjoy better and better rewards!
                                        </p>
                                        <p>
                                            Looking forward to seeing you soon!
                                        </p>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left" valign='top'>
                                        <p style='margin: 1em 0;'>
                                            All the best,
                                            <br>
                                            {{ $extra['store_name'] }}
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </center>
        </td>
    </tr>
@stop
