<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Choose Plan</title>
    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="{{asset('css/app.css')}}" type="text/css" />

    @if(config('shopify-app.appbridge_enabled'))
        <script src="https://unpkg.com/@shopify/app-bridge"></script>
        <script>
            var AppBridge = window['app-bridge'];
            var createApp = AppBridge.default;

            window.shopify_app_bridge = createApp({
                apiKey: '{{ config('shopify-app.api_key') }}',
                shopOrigin: '{{ Auth::user()->name }}',
                forceRedirect: true,
            });
        </script>
    @endif

</head>
<body class="">
<div class="wrapper back-to-home-btn">


    <a href="/" class="back-to-home-btn-inner">Back to Home</a>

    <h1 class="text-center">CHOOSE SUBSCRIPTION PLAN</h1>


</div>

<div class="choose-plan-row">
    <div class="col">
        <div class="card">
            <div class="card-header">
                <h2 class="title">Loyalty Starter</h2>
                <p class="price">
                    0
                    <span>{{ config('shopify-app.currency_symbol') }}</span>
                </p>
                <span class="description">Free Plan</span>
                <div class="line"></div>
            </div>

            <ul class="features">
                <li><i class="fa fa-check"></i> Easy to install app on Shopify</li>
                <li><i class="fa fa-check"></i> Easy to use Dashboard for full control</li>
                <li><i class="fa fa-check"></i> 100 customers</li>
            </ul>
            @if(\Auth::user()->plan_id == 1)
                <div class="alert alert-success">
                    Current Plan
                </div>
            @else
                <a href="{{ route('choose.plan.free')}}" target="_blank" class="btn btn-success">Select</a>
            @endif
        </div>
    </div>
    <div class="col">
        <div class="card">
            <div class="card-header">
                <h2 class="title">Online Loyalty</h2>
                <p class="price">
                    49
                    <span>{{ config('shopify-app.currency_symbol') }}</span>
                </p>
                <span class="description">Perfect for Online Store</span>
                <div class="line"></div>
            </div>

            <ul class="features">
                <li><i class="fa fa-check"></i> Easy to install app on Shopify</li>
                <li><i class="fa fa-check"></i> Easy to use Dashboard for full control</li>
                <li><i class="fa fa-check"></i> 10,000 customers</li>
                <li><i class="fa fa-check"></i> White Label - Use your colours and your logo</li>
                <li><i class="fa fa-check"></i> Email support</li>
            </ul>
            @if(\Auth::user()->plan_id == 2)
                <div class="alert alert-success">
                    Current Plan
                </div>
            @else
                <a href="{{ route('billing', ['plan' => 2])}}" target="_blank" class="btn btn-success">Select</a>
            @endif
        </div>
    </div>
    <div class="col">
        <div class="card">
            <div class="card-header">
                <h2 class="title">Online and Offline</h2>
                <p class="price">
                    199
                    <span>{{ config('shopify-app.currency_symbol') }}</span>
                </p>
                <span class="description">Perfect for physical stores with online presence</span>
                <div class="line"></div>
            </div>

            <ul class="features">
                <li><i class="fa fa-check"></i> Easy to install app on Shopify</li>
                <li><i class="fa fa-check"></i> Easy to use Dashboard for full control</li>
                <li><i class="fa fa-check"></i> Unlimited customers</li>
                <li><i class="fa fa-check"></i> White Label - Use your colours and your logo</li>
                <li><i class="fa fa-check"></i> iOS and Anroid integration (custom mobile app available)</li>
                <li><i class="fa fa-check"></i> Monthly reporting</li>
                <li><i class="fa fa-check"></i> POS integration</li>
                <li><i class="fa fa-check"></i> 24/7 Customer service</li>
            </ul>
            @if(\Auth::user()->plan_id == 3)
                <div class="alert alert-success">
                    Current Plan
                </div>
            @else
                <a href="{{ route('billing', ['plan' => 3])}}" target="_blank" class="btn btn-success">Select</a>
            @endif
        </div>
    </div>
    <div class="col">
        <div class="card">
            <div class="card-header">
                <h2 class="title">Enterprise</h2>
                <p class="price">
                    499
                    <span>{{ config('shopify-app.currency_symbol') }}</span>
                </p>
                <span class="description">For more 5+ stores and heavy online presence</span>
                <div class="line"></div>
            </div>

            <ul class="features">
                <li><i class="fa fa-check"></i> Easy to install app on Shopify</li>
                <li><i class="fa fa-check"></i> Easy to use Dashboard for full control</li>
                <li><i class="fa fa-check"></i> Unlimited customers</li>
                <li><i class="fa fa-check"></i> White Label - Use your colours and your logo</li>
                <li><i class="fa fa-check"></i> iOS and Anroid integration (custom mobile app available)</li>
                <li><i class="fa fa-check"></i> Live reporting</li>
                <li><i class="fa fa-check"></i> Custom POS integration</li>
                <li><i class="fa fa-check"></i> 24/7 Customer service</li>
                <li><i class="fa fa-check"></i> Personal Assistant and Web Training</li>
            </ul>

            @if(\Auth::user()->plan_id == 4)
                <div class="alert alert-success">
                    Current Plan
                </div>
            @else
                <a href="{{ route('billing', ['plan' => 4])}}" target="_blank" class="btn btn-success">Select</a>
            @endif
        </div>
    </div>
</div>




</body>
</html>
