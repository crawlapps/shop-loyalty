@extends('layouts.app')
@section('content')
    <display-setting
        props-permissions='{!!
                        json_encode([

                        ],JSON_HEX_APOS)!!}'
        props-urls='{!!
                        json_encode([
                                    "index" => route("display-setting.index",["api"=>1]),
                                    "store" => route("display-setting.index"),
                                    "edit" => route("display-setting.edit",[getShop()->id]),

                        ],JSON_HEX_APOS)
                    !!}'
        props-data='{!!
                        json_encode([
                            'shop' => \Auth::user(),
                            'featurePlanLogo' => checkPlanFeatures()['logo'],
                        ],JSON_HEX_APOS)
                    !!}'
    >
    </display-setting>
@endsection
