@extends('layouts.app')
@section('content')
    <customer-level
        props-permissions='{!!
                        json_encode([

                        ],JSON_HEX_APOS)!!}'
        props-urls='{!!
                        json_encode([
                                    "index" => route("customer-level.index",["api"=>1]),
                                    "store" => route("customer-level.store"),
                                    "edit" => route("customer-level.edit",["__"]),
                                    "update" => route("customer-level.update",["__"]),
                                    "discount_code" => route("discount.code"),
                        ],JSON_HEX_APOS)
                    !!}'
        props-data='{!!
                        json_encode([
                            'shop' => \Auth::user(),
                        ],JSON_HEX_APOS)
                    !!}'
    >
    </customer-level>
@endsection
