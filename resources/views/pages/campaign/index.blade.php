@extends('layouts.app')
@section('content')
    <campaign
        props-permissions='{!!
                        json_encode([

                        ],JSON_HEX_APOS)!!}'
        props-urls='{!!
                        json_encode([
                                    "index" => route("campaign.index",["api"=>1]),
                                    "store" => route("campaign.store"),
                                    "edit" => route("campaign.edit",["__"]),
                                    "update" => route("campaign.update",["__"]),
                                    "discount_code" => route("discount.code"),
                        ],JSON_HEX_APOS)
                    !!}'
        props-data='{!!
                        json_encode([
                                    'shop' => \Auth::user(),
                        ],JSON_HEX_APOS)
                    !!}'
    >
    </campaign>
@endsection
