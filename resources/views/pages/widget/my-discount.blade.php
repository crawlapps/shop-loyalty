<form action="{{ route('customer.discount',[$shop->name,'customer' => $customer_id]) }}" method="POST" class="text-left">
    @csrf
    <select class="form-control mb-1" id="select-change" name="selected_discount">
        <option value="">-- Select Discount --</option>
        @foreach($discounts as $key => $val)
            <option value="{{$val->id}}" selected="{{  ($customer->discount_id == $val->id)?'selected':''  }}" data-summary="{{$val->summary}}">{{$val->discount_code}}</option>
        @endforeach
    </select>
    <input type="text" disabled="" class="form-control mb-1" id="summary" value="{{$customer_discount}}"/>
    <button type="button" id="apply" class="btn btn-sm theme-bg btn-primary btn-block" style="font-size: 13px">{{ __('Apply') }}</button>
</form>
<script>
    $(document).ready(function (){
        $("#select-change").on('change',function(){
                $("#summary").val($(this).find(':selected').data('summary'));
        });
        $("#apply").on('click',function(){
            let url = "https://{{$shop->name}}/discount/"+$('#select-change').find(':selected').text();
            window.open(url);
            return true;
            /*$.ajax({
                method: "GET",
                url: url,
                contentType: 'text/html;',
                success:function (response,success,header) {
                    console.log(response);
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    console.log(errorThrown);
                },
            });*/

        });
    });
</script>
