<!-- JQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.1/js/mdb.min.js"></script>
<script type="text/javascript">
    var loc = window.location.href;
    function handleAJAX(url, params = '')
    {
        $('#loader').show();
        $.ajax({
            url: url + parent.cus_attached_params + params,
            success: function(result){
                $("#page-content").html(result);
                $('#loader').hide();
                $('#ajax-error').fadeOut();
            },
            error: function(){
                $('#ajax-error').fadeIn();
                setTimeout(function(){
                    $('#ajax-error').fadeIn();
                }, 5000);
                $('#loader').hide();
            }
        });
    }
    $(document).on('click', 'a.action', function(e){
        e.preventDefault();
        var url = $(this).attr('href');
        handleAJAX(url);
    });
    $('.reward-toggler').click(function(){
        if(loc.indexOf('campaign_type=email') !== -1){
            var w = '100%';
            $('#shoployal-frame', window.parent.document).css({
                "background": "rgba(0,0,0,.6)"
            });
            $('#modalDiscount').css({'height': '100%'});
            $('#modalDiscount .modal-dialog').removeClass('modal-side');
        }
        else{
            var w = '300px';
            $('#shoployal-frame', window.parent.document).css({
                "background": "none"
            });
            $('#modalDiscount').css({'height': 'auto'});
            $('#modalDiscount .modal-dialog').addClass('modal-side');
        }
        $('#shoployal-frame', window.parent.document).css({"height":"100%", "width": w});
    });
    $(document).on('hidden.bs.modal', '#modalDiscount', function(){
        $('#shoployal-frame', window.parent.document).css({
            "height":"100px", "width": "240px",
            "background": "none"
        });
    });

</script>
