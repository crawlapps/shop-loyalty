<!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
<!-- Google Fonts -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
<!-- Bootstrap core CSS -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.1/css/mdb.min.css" rel="stylesheet">

<style type="text/css">
    body{
        font-family: "Quicksand";
        background-color: inherit;
    }
    .hide{
        display: none !important;
    }
    .modal-body{
        max-height: 450px;
        overflow-y: scroll;
        overflow-x: hidden;
        padding: 10px !important;
    }
    .modal-footer{
        display: block;
    }
    .list-item-custom{
        background-color: #f9f9f9;
        border-radius: 4px;
        padding: 0px;
    }
    .list-item-custom .icon{
        font-size: 23px;
        padding-top: 4px;
        padding-left: 15px
    }
    .list-item-custom .image{
        width: 70px;
    }
    .list-item-custom .desc{
        padding-left: 15px;
        padding-right: 15px;
        padding-top: 5px;
    }
    .list-item-custom .desc h6{
        font-size: 14px;
    }
    .list-item-custom .desc h6 span{
        text-overflow: ellipsis;
        overflow: hidden;
        white-space: nowrap;
    }
    .list-item-custom .desc h6 b{
        font-weight: bold;
    }
    .list-item-custom .desc small{
        color: gray;
    }
    .list-item-custom .badge{
        position: absolute;
        right: 15px;
        margin-top: 2px;
        box-shadow: none;
    }
    .custom-pills a{
        border-bottom: 2px solid transparent;
        color: #ff3547;
    }
    .custom-pills a.active{
        border-bottom: 2px solid #ff3547;
        font-weight: bold;
    }
    .inner-heading{
        font-weight: bold;
        font-size: 14px;
        letter-spacing: 2px;
        margin-bottom: 10px;
    }
    .inner-heading .fas{
        color: #616161;
    }
    #loader{
        display: flex;
        justify-content: center;
        position: absolute;
        left: 0;
        top: 0;
        height: 100%;
        width: 100%;
        padding-top: inherit;
        background: rgba(255,255,255,.8);
    }
    .reward-toggler{
        position: fixed;
        right: 30px;
        bottom: 14px;
        z-index: 10;
    }
    .active-discount a{
        display: inherit;
    }
    .active-clevel{
        background: #e2e2e2;
    }
    .active-clevel .desc{
        border-left: 4px solid #004d9a !important;
    }
    .active-clevel .desc, .active-clevel .desc small{
        color: #004d9a;
    }
    .list-item-custom.clevel .desc{
        border-left: 4px solid #ccc;
    }
    table.table th{
        font-weight: 500;
    }
    table.table td, table.table th{
        padding: 4px;
        font-size: 12px;
    }
    .table-bordered td, .table-bordered th {
        border: 1px solid #ddd;
    }
    table.table td .btn{
        padding: 1px 5px !important;
    }
    .table-responsive h6{
        font-size: 14px;
    }

    .theme-bg{
        background: {{ $settings->common_data['section_header_bg_color'] }}
    }

    .theme-color{
        color: {{ $settings->common_data['section_header_color'] }}
    }

    .theme-border{
        border:1px solid #004d9a !important;
    }
    .fw500{
        font-weight: 500;
    }
    .p-84rem{
        padding: .84rem;
    }
    .blabel{
        font-weight: bold;margin-bottom: 2px;font-size: 12px;
    }
    .binput{
        font-size: 13px;
    }
    .alert-danger,.alert-success{
        padding: 5px 10px;
        font-size: 12px;
        font-weight: 500;
        margin-bottom: 10px;
    }


    @if(!empty($settings->common_data['section_header_bg_color']))
        .section_header_bg{
           background: {{ $settings->common_data['section_header_bg_color'] }} !important;
        }
    @endif

    @if(!empty($settings->common_data['section_header_color']))
        .section_header_color{
            color: {{ $settings->common_data['section_header_color'] }} !important;
        }
    @endif

    @if(!empty($settings->common_data['btn_bg_color']))
        .btn_bg_color{
            background: {{ $settings->common_data['btn_bg_color'] }} !important;
        }

        .btn_bg_color:not([disabled]):not(.disabled):active, .btn_bg_color:not([disabled]):not(.disabled).active, .show>.btn_bg_color.dropdown-toggle{
            background: {{ $settings->common_data['btn_bg_color'] }} !important;
        }
    @endif

    @if(!empty($settings->common_data['btn_text_color']))
        .btn_text_color{
            color: {{ $settings->common_data['btn_text_color'] }} !important;
        }
    @endif

    @if(!empty($settings->common_data['btn_border_size']))
        .btn_border_size{
            border-width: {{ $settings->common_data['btn_border_size'] }}px !important;
        }
    @endif

    @if(!empty($settings->common_data['btn_border_color']))
        .btn_border_color{
            border-color: {{ $settings->common_data['btn_border_color'] }} !important;
            border-style: solid !important;
        }
    @endif

    .content-body table tr td{
        padding:10px;
    }
    .my-discounts{
        list-style: none;
    }
</style>
<script
    src="https://code.jquery.com/jquery-3.5.1.min.js"
    integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
    crossorigin="anonymous"></script>
