@include('pages.widget.header')

@if(checkPlanFeatures($shop)['logo'])
    @if($settings->logo_path)
    <img src="{{ asset('storage/widget/'.$settings->logo_path) }}"  class="reward-toggler" style="width: 85px;cursor: pointer" data-toggle="modal" data-target="#modalDiscount" />
     @else
    <img src="{{config('app.url')}}/images/widget.svg"  class="reward-toggler" style="width: 85px;cursor: pointer" data-toggle="modal" data-target="#modalDiscount" />
    @endif
@else
    <img src="{{config('app.url')}}/images/widget.svg"  class="reward-toggler" style="width: 85px;cursor: pointer" data-toggle="modal" data-target="#modalDiscount" />
    {{--<a style="cursor:pointer;" class="reward-toggler" data-toggle="modal" data-target="#modalDiscount">{{ $settings->widget_text }}</a>--}}
@endif

<div class="modal fade right" id="modalDiscount" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="false" style="height: auto;bottom: 0px;top: auto;">
    <div class="modal-dialog modal-side modal-bottom-right modal-notify modal-danger modal-sm" role="document">
        <!--Content-->
        <div class="modal-content">
            <!--Header-->
            <div class="modal-header section_header_bg">
                <p class="heading fw500 section_header_color">
                    {{ $settings->common_data['section_header_text']  }}
                </p>
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="white-text section_header_color">&times;</span>
                </button>
            </div>

            <!--Body-->
            @if($is_login==1)
                <div class="modal-body">
                <div class="alert alert-danger" id="ajax-error" role="alert" style="display: none">
                    {{ __('Something went wrong, please try again') }}.
                </div>
                @if(!empty($joined))
                    <div class="alert alert-success" id="joining">
                        {{ __('Thank you for joining') }}.
                    </div>
                @endif
                <div id="page-content">
                    @if(!empty($forJoining))
                        @if(!checkPlanFeatures($shop)['customers'])
                            <div class="alert alert-danger" id="joining-error" role="alert" style="display: none">
                                {{ __('Please contact to admin about this.') }}
                            </div>
                        @else
                        <div class="alert alert-danger" id="joining-error" role="alert" style="display: none">
                            {{ __('Accept terms & conditions to continue') }}.
                        </div>
                        <div class="mt-1 mb-1 text-center">
                            <h5 class="mb-3"><b>{{ __('Join Reward Program & Start Earning Now') }}</b></h5>
                            <form action="{{ route('customer.join',[$shop->name]) }}" class="text-left">
                                <label class="blabel mb-2" style="font-size: 13px;font-weight: normal">
                                    {{ __('Loyalty card code is optional and not required. You only need to get a Loyalty Card in the store.If you cannott find a problem now, you can add it to your account later in up to two cases') }}
                                </label>
                                <input type="text" min="13" class="form-control mb-1" placeholder="{{ __('Add Barcode') }}" name="barcode" style="font-size: 13px">
                                <input type="hidden" class="form-control mb-1" name="customer" value="{{$customer_id}}" style="font-size: 13px">
                                <div class="custom-control custom-checkbox mt-2">
                                    <input type="checkbox" class="custom-control-input" id="defaultUnchecked" name="terms_accepted" value="1">
                                    <label class="custom-control-label blabel" for="defaultUnchecked"><b><a target="_blank" href="https://jateksziget.hu/pages/jateksziget-husegklub">{{ __('I accept terms & conditions') }}.</a></b></label>
                                </div>
                                <div class="custom-control custom-checkbox mb-2">
                                    <input type="checkbox" class="custom-control-input" id="defaultUnchecked1" name="terms_accepted_1" value="1">
                                    <label class="custom-control-label blabel" for="defaultUnchecked1"><b><a target="_blank" href="https://jateksziget.hu/pages/adatvedelem">Szeretnék hírlevelet kapni - elolvastam és elfogadom az Adatkezelési tájékoztatót.</a></b></label>
                                </div>
                                <button class="btn btn-sm theme-bg btn-primary btn-block" style="font-size: 13px">{{ __('Join Now') }}</button>
                            </form>
                        </div>
                        @endif
                    @else
                        <div class="mt-1 mb-1 text-center content-body">
                            <h5 class="mb-3"><b>{{ __('My Account') }}</b></h5>
                            @if(@$page == "my-discount")
                                @include('pages.widget.my-discount')
                            @else
                                @include('pages.widget.my-info')
                            @endif
                        </div>
                    @endif
                </div>
                <div class="justify-content-center" id="loader" style="display: none">
                    <div class="spinner-border theme-color" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>
                </div>
            </div>
            @endif

            @if($is_login==0)
                <div class="modal-footer">
                    <a href="https://{{$shop->name}}/account/register?_loy=_sty128Rtyse7I5uY" target="_top" class="btn btn-danger waves-effect btn-block btn_bg_color btn_text_color btn_border_size btn_border_color mb-2">{{ __('Create Store Account') }}</a>
                    <a href="https://{{$shop->name}}/account" target="_top" class="login-href btn btn-danger waves-effect btn-block btn_bg_color btn_text_color btn_border_size btn_border_color">{{ __('Log In') }}
                    </a>
                </div>
            @endif
        </div>
        <!--/.Content-->
    </div>
</div>
@extends('pages.widget.footer')
