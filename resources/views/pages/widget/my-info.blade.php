<table style="width: 100%;">
    <tbody>
    <tr>
        <td>Name:</td>
        <td>{{ $customer->name }}</td>
    </tr>
    <tr>
        <td>Email:</td>
        <td>{{ $customer->email }}</td>
    </tr>
    <tr>
        <td>Loyalty Number:</td>
        <td>{{ $customer->loyalty_number }}</td>
    </tr>
    <tr>
        <td>Total Points:</td>
        <td>{{ $customer->points }}</td>
    </tr>
    <tr>
        <td>Spent:</td>
        <td>{{ $customer->spent }}</td>
    </tr>
    <tr>
        <td>Balance:</td>
        <td>{{ $customer->balance }}</td>
    </tr>
    <tr>
        <td>Current Level:</td>
        <td>{{ @$current_level->blg_customer_level->label }}</td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: center;"><a href="{{ route('customer.discount',[$shop->name,'customer' => $customer_id]) }}">View My Discounts</a></td>
    </tr>
    </tbody>
</table>
