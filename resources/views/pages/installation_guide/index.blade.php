<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Choose Plan</title>
    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="{{asset('css/app.css')}}" type="text/css" />
    @if(config('shopify-app.appbridge_enabled'))
        <script src="https://unpkg.com/@shopify/app-bridge"></script>
        <script>
            var AppBridge = window['app-bridge'];
            var createApp = AppBridge.default;

            window.shopify_app_bridge = createApp({
                apiKey: '{{ config('shopify-app.api_key') }}',
                shopOrigin: '{{ Auth::user()->name }}',
                forceRedirect: true,
            });
        </script>
    @endif
<style>
    .meta-config-install-img{
        width: 100%;
    }
    ul li{
        list-style: none;
    }
</style>
</head>
<body class="">
<div class="wrapper back-to-home-btn">
    <a href="/" class="back-to-home-btn-inner">Back to Home</a>
    <h1 class="text-center">Installation Guide</h1>
</div>

<div>
    <div class="container-fluid text-center">
        <div class="row content">
            <div class="col-sm-12 text-left">
                <div class="accordion" id="metafieldsConfiguration">

                    <div class="card configurations" data-toggle="collapse" data-target="#collapse-configurations" aria-expanded="true" aria-controls="collapseConfigurations">
                        <div class="card-header" id="configurationsOne" style="cursor:pointer;">
                            <h5 class="mb-0">
                                    Select Plan and Pricing
                            </h5>
                        </div>

                        <div id="collapse-configurations" class="collapse show" aria-labelledby="configurationsOne" data-parent="#metafieldsConfiguration">
                            <div class="card-body">
                                <ul>
                                    <img src="/images/installation_guide/plan-pricing.png" class="meta-config-install-img">
                                </ul>
                            </div>
                        </div>
                    </div>

                    {{-- Earning Rules --}}
                    <div class="card settings" data-toggle="collapse" data-target="#collapse-settings" aria-expanded="true" aria-controls="collapseOne" >
                        <div class="card-header" id="configurationsOne" style="cursor:pointer;">
                            <h5 class="mb-0">
                                    Earning Rules
                             </h5>
                        </div>

                        <div id="collapse-settings" class="collapse show" aria-labelledby="configurationsOne" data-parent="#metafieldsConfiguration">
                            <div class="card-body">
                                <ul>

                                    <li>
                                        <p>Store admin must have to select atleast one earning rules</p>
                                        <img src="/images/installation_guide/earning_rules-2.png" class="meta-config-install-img">
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>

                    {{-- Dashboard --}}
                    <div class="card exported-csv" data-toggle="collapse" data-target="#collapse-exported-csv" aria-expanded="true" aria-controls="collapseOne">
                        <div class="card-header" id="exported-csvOne" style="cursor:pointer;">
                            <h5 class="mb-0">
                                    Dashboard
                            </h5>
                        </div>

                        <div id="collapse-exported-csv" class="collapse show" aria-labelledby="exported-csvOne" data-parent="#metafieldsConfiguration">
                            <div class="card-body">
                                <ul>
                                    <li>
                                        <img src="/images/installation_guide/dashboard-3.png" class="meta-config-install-img">
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    {{--Customers--}}
                    <div class="card shop" data-toggle="collapse" data-target="#collapse-shop" aria-expanded="true" aria-controls="collapseOne">
                        <div class="card-header" id="shopOne" style="cursor: pointer;">
                            <h5 class="mb-0">
                                    Customers
                            </h5>
                        </div>

                        <div id="collapse-shop" class="collapse show" aria-labelledby="shopOne" data-parent="#metafieldsConfiguration">
                            <div class="card-body">
                                <ul>
                                    <li>
                                        <img src="/images/installation_guide/customer-4.png" class="meta-config-install-img">
                                    </li>
                                    <li>
                                        <img src="/images/installation_guide/customer-5.png" class="meta-config-install-img">
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    {{--Customers--}}
                    <div class="card" data-toggle="collapse" data-target="#cutomer-level" aria-expanded="true" aria-controls="collapseOne">
                        <div class="card-header" id="shopOne" style="cursor: pointer;">
                            <h5 class="mb-0">
                                    Customer Level
                            </h5>
                        </div>

                        <div id="cutomer-level" class="collapse show" aria-labelledby="shopOne" data-parent="#metafieldsConfiguration">
                            <div class="card-body">
                                <ul>
                                    <li>
                                        <img src="/images/installation_guide/level-6.png" class="meta-config-install-img">
                                    </li>
                                    <li>
                                        <p>
                                            Here, you need to set Level name, Discount code, Summary, Required Points
                                        </p>
                                        <img src="/images/installation_guide/level-7.png" class="meta-config-install-img">
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    {{--Campaign--}}
                    <div class="card" data-toggle="collapse" data-target="#campaign" aria-expanded="true" aria-controls="collapseOne">
                        <div class="card-header" id="shopOne" style="cursor: pointer;">
                            <h5 class="mb-0">
                                    Campaign
                            </h5>
                        </div>

                        <div id="campaign" class="collapse show" aria-labelledby="shopOne" data-parent="#metafieldsConfiguration">
                            <div class="card-body">
                                <ul>
                                    <li>
                                        <img src="/images/installation_guide/campaign-8.png" class="meta-config-install-img">
                                        <img src="/images/installation_guide/campaign-8.0.png" class="meta-config-install-img">
                                        <p>- First step is to add a Name, Start and End date to a new campaign. It is advisable to select a meaningful name for the campaign.</p>
                                        <p>- As this name is for your reference, it is good practice to include crucial information to help at a glance. For example (please check if special characters won’t cause issues, such as % or -)“Halloween -10 % - Level1”, or “New Joiner Campaign – 10%”</p>
                                        <p>- End date is not mandatory, and can be skipped, if the Campaign is scheduled to run indefinitely.</p>
                                        <p>- Campaign can be enabled/disabled with the status button.</p>


                                        <p>- Once the naming and run time determined, the most important part is to set the Target of our campaign.</p>
                                        <p>- There are two type of Campaigns available, Campaign and Offer.</p>
                                        <p> <b>Campaign Type</b> <br>
                                            - First step is selecting the Customer level from the dropdown. This dropdown contains all the customer levels already set up under Customers/Customer Levels menu.</p>

                                        <p>- Campaign type is targeted to a group of customers, with selected age range and gender within the Customer Level group. For example, one can set that “Every 20-30 Male in Level 1” will receive the discount provided. This will help reach crucial customer segments and fine tune the reward system.</p>
                                        <p>- Earning Points are further customizing the campaign. We can set when the discount applies, on the customer’s Birthday, when the customer reaches a new level, at every shopping or at every visit.
                                        </p>
                                        <p>- Shopping is considered as a successful shopping, when the Customer reaches the check out page and pays for the goods.</p>
                                        <p>- A New Level is when a customer gains enough point to move to the next level.</p>
                                        <p>- Visit is considered a login to the website, with a maximum one per day</p>
                                        <p>- On the above example, a store wants to encourage the 20-30 age group male visitors to buy more on their Birthday.</p>
                                        <img src="/images/installation_guide/campaign-9.1.png" class="meta-config-install-img">
                                        <p> <b>Offer Type</b> <br>
                                            - Offer Type is targeted to a customer level and an Action Earning Points.</p>
                                        <p>- Offers are especially useful if the coupon code applies to a Shopify collection. The naming can help guide the user/store owner.</p>

                                        <img src="/images/installation_guide/campaign-9.2.png" class="meta-config-install-img">
                                        <p>- The final step of the process is to apply the Discount Rule to the campaign/offer.
                                            <small><i>Please note that the Discount Code field automatically populates form the Shopify-Discounts Menu, therefore the Discount must be set up first. </i></small>
                                        </p>
                                        <p>- <small>Please note that the time delay between adding the discount code to Shopify and the discount code appearing in our system is 1 hours.</small></p>
                                        <p>- The main criteria for the Discount Code is to have Customer eligibility set to Specific Customers. Please add the first customer email address – usually your own email address, or info@yoursite.com or something similar – as our system will automatically add the new customers who are eligible to this list, but the first has to be manually added ( this is a Shopify limitation to make sure no one can trick the system).</p>

                                        <img src="/images/installation_guide/campaign-9.3.png" class="">
                                        <p>- Summary will be automatically populated once the desired discount selected. Details are pulled from the Shopify – Discounts menu. </p>
                                        <p>- The system can be further customized to send the Campaign/Offer to the desired group. For example if the Level 1 is between 200-300 points, and the store owner want to encourage those who are half way to the next level, the 250 point cut off makes the Campaign/Offer only available to those who have equal to or more than 250 point on the above example.</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    {{--Display Setting--}}
                    <div class="card" data-toggle="collapse" data-target="#display_setting" aria-expanded="true" aria-controls="collapseOne">
                        <div class="card-header" id="shopOne" style="cursor: pointer;">
                            <h5 class="mb-0">
                                    Display Setting
                            </h5>
                        </div>

                        <div id="display_setting" class="collapse show" aria-labelledby="shopOne" data-parent="#metafieldsConfiguration">
                            <div class="card-body">
                                <ul>
                                    <li>
                                        <img src="/images/installation_guide/display_setting-12.png" class="meta-config-install-img">
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
<script>
    $('.collapse').collapse()
</script>
