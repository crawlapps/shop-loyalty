@extends('layouts.app')
@section('content')
    <earning-points
        props-permissions='{!!
                        json_encode([

                        ],JSON_HEX_APOS)!!}'
        props-urls='{!!
                        json_encode([
                                    "index" => route("earning-points.index",["api"=>1]),
                                    "edit" => route("earning-points.edit",["__"]),
                                    "update" => route("earning-points.update",["__"]),
                        ],JSON_HEX_APOS)
                    !!}'
        props-data='{!!
                        json_encode([
                            'shop' => \Auth::user(),
                        ],JSON_HEX_APOS)
                    !!}'
    >
    </earning-points>
@endsection
