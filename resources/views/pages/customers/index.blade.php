@extends('layouts.app')
@section('content')
    <customers
        props-permissions='{!!
                        json_encode([

                        ],JSON_HEX_APOS)!!}'
        props-urls='{!!
                        json_encode([
                                    "index" => route("customers.index",["api"=>1]),
                                    "store" => route("customers.store"),
                                    "edit" => route("customers.edit",["__"]),
                                    "update" => route("customers.update",["__"]),
                        ],JSON_HEX_APOS)
                    !!}'
        props-data='{!!
                        json_encode([
                            'shop' => \Auth::user(),
                        ],JSON_HEX_APOS)
                    !!}'
    >
    </customers>
@endsection
