@extends('layouts.app')
@section('content')
    <dashboard
        props-permissions='{!!
                        json_encode([

                        ],JSON_HEX_APOS)!!}'
        props-urls='{!!
                        json_encode([
                            "index" => route("home",["api"=>1]),
                            "customer_index" => route("customers.index",["api"=>1]),
                        ],JSON_HEX_APOS)
                    !!}'
        props-data='{!!
                        json_encode([
                            'shop' => \Auth::user(),
                        ],JSON_HEX_APOS)
                    !!}'
    >
    </dashboard>
@endsection
