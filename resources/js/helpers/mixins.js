export default {
    gaugeChart() {
        return {
            type: "gauge",
            "background-color":"rgba(255, 255, 255, 0)",
            theme: 'light',
            plotarea: {
                marginTop: 80,
            },
            plot: {
                size: '90%',
            },
            tooltip: {
                visible: true
            },
            scaleR: {
                aperture: 180,
                minValue: 0,
                maxValue: 100,
                step: 1,
                center: {
                    visible: false
                },
                tick: {
                    visible: false
                },
                item: {
                    offsetR: 0,
                    rules: [{
                        rule: '%i == 9',
                        offsetX: 15
                    }]
                },
                labels: ["0"],
                ring: {
                    size: 20,
                    backgroundColor: '#3c8dbc'
                }
            }
        };
    },
    pieChart(){
        return {
            type: "pie3d",
            series: []
        };
    },
};
