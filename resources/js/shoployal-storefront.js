const shoployal_base_url = process.env.MIX_APP_URL;
const apiEndPoint = shoployal_base_url + '/api/';

var shopifyDomain = Shopify.shop;
var frontendDomain = window.location.origin;
var jqueryLoaded = 0;
if(!window.jQuery)
{

    var script = document.createElement('script');
    script.type = "text/javascript";
    script.src = "https://code.jquery.com/jquery-3.5.1.min.js";
    document.getElementsByTagName('head')[0].appendChild(script);
    jqueryLoaded = 1;
}

var cus_attached_params = "", url = "", base = "";
Window.shoployalStoreFront = {
    shop_data: JSON.parse(document.getElementById('crawlapps_shoployal_data').innerHTML),
    init:function() {
        base = this;
        let myParam = getParameterByName('_loy',window.location.href);

        if(myParam == "_sty128Rtyse7I5uY"){
            let inputTag = document.createElement('input');
            inputTag.type = "hidden";
            inputTag.name = "customer[tags]";
            inputTag.value = "shopLoyal-customer";
            let form  = document.querySelector("form[action='/account']");
            form.appendChild(inputTag);
        }


        var current_url = window.location.href;
        if(current_url.indexOf('uniqueId') > 0)
        {
            var pieces = current_url.split('uniqueId=');
            if(pieces[1] && pieces[1] != '')
            {
                setCookie(pieces[1]);
            }
        }

        var shopURL = window.location.host;
        var iframe = document.createElement('iframe');

        iframe.id = "shoployal-frame";
        iframe.style.position = "fixed";
        iframe.style.right = "0px";
        iframe.style.bottom = "0px";
        iframe.style.zIndex = "10";
        iframe.style.height = "100px";
        iframe.style.width = "240px";
        iframe.style.border = "none";
        if(typeof __st.cid != 'undefined')
        {
            url = apiEndPoint + shopifyDomain + '/widget/dashboard?customer=' + __st.cid + '&required=layout';
            cus_attached_params = '?shop='+shopURL + '&customer=' + __st.cid+"&is_login=1";
        }
        else
        {
            url = apiEndPoint + shopifyDomain + '/widget?is_login=0';
            cus_attached_params = '?shop='+shopifyDomain;
        }

        document.body.appendChild(iframe);

        base.initFrame();

    },
    initFrame(){
        if(typeof __st.cid !== 'undefined' && getCookie("SL_unique_id") !== "")
        {

            //let cust_url = aPIEndPoint + shopifyDomain + "/save-app-user"+cus_attached_params+"&unique_id=" + getCookie("SL_unique_id")
            $.ajax({
                method: "GET",
                url: url,
                contentType: 'application/json;',
                success:function (response,success,header) {
                    document.cookie = "SL_unique_id" + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                    if(response != '')
                    {
                        var iframeDoc = $('#shoployal-frame')[0].contentDocument;
                        iframeDoc.write(response);
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    console.log(errorThrown);
                },
            });
        }

        $.ajax({
            url: url,
            success: function(result){
                var iframeDoc = $('#shoployal-frame')[0].contentDocument;
                iframeDoc.write(result);
            }
        });
    }
};



if(jqueryLoaded){
    setTimeout(function(){
        $(document).ready(function () {
            Window.shoployalStoreFront.init();
        });
    },2000);
}else{
    $(document).ready(function () {
        Window.shoployalStoreFront.init();
    });
}

function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2)
        return parts.pop().split(";").shift();
    return "";
}

function setCookie(value){
    var exdate=new Date();
    exdate.setDate(exdate.getDate() + 1);
    var c_value=escape(value) + "; expires="+exdate.toUTCString()
        + "; path=/";
    document.cookie="SL_unique_id" + "=" + c_value;
}


function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}
