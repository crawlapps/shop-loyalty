import Error from "../../../helpers/Error";

export default {
    /**
     * To Set Form Validation Error
     * @param state
     * @param errors
     */
    setError(state, errors) {
        state.errors = new Error(errors);
    },

    initError(state) {
        state.errors = new Error({});
    },

    /**
     * To Set Amenity Edit time States
     * @param state
     * @param data
     */
    setStates(state, data) {
        state.formFields.id = data.id;
        state.formFields.label = data.label;
        state.formFields.status = !! data.status;
        state.formFields.type = data.type;
        state.formFields.start_at = data.start_at;
        state.formFields.end_at = data.end_at;
        state.formFields.customer_level_id = data.customer_level_id;
        state.formFields.customer_age_from = data.customer_age_from;
        state.formFields.customer_age_to = data.customer_age_to;
        state.formFields.customer_gender = data.customer_gender;
        state.formFields.earning_point_id = data.earning_point_id ? data.earning_point_id : '';
        state.formFields.require_points = data.require_points;
        state.formFields.discount_id = data.discount_id ? data.discount_id :'';
    },

    /**
     * To default init State
     * @param state
     */
    initStates(state) {
        state.formFields.id = '';
        state.formFields.label = '';
        state.formFields.status = true;
        state.formFields.type = 'campaign';
        state.formFields.start_at = '';
        state.formFields.end_at = '';
        state.formFields.customer_level_id = '';
        state.formFields.customer_age_from = '';
        state.formFields.customer_age_to = '';
        state.formFields.customer_gender = 'male';
        state.formFields.earning_point_id = '';
        state.formFields.require_points = '';
        state.formFields.discount_id = '';
        state.errors = new Error({});
    },

}
