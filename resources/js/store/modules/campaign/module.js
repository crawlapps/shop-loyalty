import mutations from './mutations';
import getters from './getters';
import Error from "../../../helpers/Error";

const module = {
    namespaced: true,
    state() {
        return {
            formFields: {
                id: '',
                label: '',
                status: true, // 1:enable, 0:disable
                type: 'campaign', // campaign, offer
                start_at: '',
                end_at: '',
                customer_level_id: '',
                customer_age_from: '',
                customer_age_to: '',
                customer_gender: 'male', //male, female, other
                earning_point_id: '',
                require_points: '',
                discount_id: '',
            },
            errors: new Error({}),
        }
    },
    mutations,
    getters,
};

export default module;
