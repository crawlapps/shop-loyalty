import Error from "../../../helpers/Error";

export default {
    /**
     * To Set Form Validation Error
     * @param state
     * @param errors
     */
    setError(state, errors) {
        state.errors = new Error(errors);
    },

        initError(state) {
        state.errors = new Error({});
    },

    /**
     * To Set Amenity Edit time States
     * @param state
     * @param data
     */
    setStates(state, data) {
        state.formFields.id = data.id;
        state.formFields.label = data.label;
        state.formFields.discount_id = data.discount_id;
        state.formFields.require_points = data.require_points;
    },

    /**
     * To default init State
     * @param state
     */
    initStates(state) {
        state.formFields.id = '';
        state.formFields.label = '';
        state.formFields.discount_id = '';
        state.formFields.require_points = '';
        state.errors = new Error({});
    },
}
