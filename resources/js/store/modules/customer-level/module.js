import mutations from './mutations';
import getters from './getters';
import Error from "../../../helpers/Error";

const module = {
    namespaced: true,
    state() {
        return {
            formFields: {
                id: '',
                label: '',
                discount_id: '',
                require_points: '',
            },
            errors: new Error({}),
        }
    },
    mutations,
    getters,
};

export default module;
