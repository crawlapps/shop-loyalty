import Error from "../../../helpers/Error";

export default {
    /**
     * To Set Form Validation Error
     * @param state
     * @param errors
     */
    setError(state, errors) {
        state.errors = new Error(errors);
    },

    /**
     * To Set Amenity Edit time States
     * @param state
     * @param data
     */
    setStates(state, data) {
        state.formFields.id = data.id;
        state.formFields.label = data.label;
        state.formFields.status = data.status;
        state.formFields.points = data.points;
        state.formFields.amount = data.amount;
        state.formFields.slug = data.slug;
    },

    /**
     * To default init State
     * @param state
     */
    initStates(state) {
        state.formFields.id = '';
        state.formFields.label = '';
        state.formFields.status = '';
        state.formFields.slug = '';
        state.formFields.points = 0;
        state.formFields.amount = 0;

        state.errors = new Error({});
    },
}
