import mutations from './mutations';
import getters from './getters';
import Error from "../../../helpers/Error";

const module = {
    namespaced: true,
    state() {
        return {
            formFields: {
                id: '',
                label: '',
                status: true,
                points: 0,
                amount: 0,
                slug: '',
            },
            errors: new Error({}),
        }
    },
    mutations,
    getters,
};

export default module;
