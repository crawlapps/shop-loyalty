export default {
    /**
     * To Get From validation Error
     * @param state
     * @returns {*}
     */
    getErrors(state) {
        return state.errors.$errors;
    },
}
