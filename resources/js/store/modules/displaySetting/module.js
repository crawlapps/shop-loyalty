import mutations from './mutations';
import getters from './getters';
import Error from "../../../helpers/Error";

const module = {
    namespaced: true,
    state() {
        return {
            formFields: {
                id: '',
                widget_text: '',
                logo_path: '',
                common_data: {
                    "section_header_text":"",
                    "section_header_color":"",
                    "section_header_bg_color":"",
                    "section_bg_color":"",
                    "section_text_color":"",
                    "btn_bg_color":"",
                    "btn_text_color":"",
                    "btn_border_radius":"",
                    "btn_border_color":"",
                    "btn_border_size":"",
                },

            },
            errors: new Error({}),
        }
    },
    mutations,
    getters,
};

export default module;
