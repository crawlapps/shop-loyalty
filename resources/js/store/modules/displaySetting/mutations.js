import Error from "../../../helpers/Error";

export default {
    /**
     * To Set Form Validation Error
     * @param state
     * @param errors
     */
    setError(state, errors) {
        state.errors = new Error(errors);
    },

    initError(state) {
        state.errors = new Error({});
    },

    /**
     * To Set Amenity Edit time States
     * @param state
     * @param data
     */
    setStates(state, data) {
        state.formFields.id = data.id;

    },

    /**
     * To default init State
     * @param state
     */
    initStates(state) {
        state.formFields.id = '';


        state.errors = new Error({});
    },
}
