import Error from "../../../helpers/Error";

export default {
    /**
     * To Set Form Validation Error
     * @param state
     * @param errors
     */
    setError(state, errors) {
        state.errors = new Error(errors);
    },

    /**
     * To Set Amenity Edit time States
     * @param state
     * @param data
     */
    setStates(state, data) {
        state.formFields.id = data.id;
        state.formFields.name = data.name;
        state.formFields.email = data.email;
        console.log(data.dob);
        console.log(moment(data.dob).format("YYYY-MM-DD"));
        state.formFields.dob = moment(data.dob).format("YYYY-MM-DD");
        state.formFields.gender = data.gender;
        state.formFields.points = data.points;
        /*state.formFields.spent = data.spent;
        state.formFields.balance = data.balance;
        state.formFields.loyalty_number = data.loyalty_number;
        state.formFields.card_barcode = data.card_barcode;
        state.formFields.card_barcode_1 = data.card_barcode_1;
        state.formFields.start_date = data.start_date;*/
    },

    /**
     * To default init State
     * @param state
     */
    initStates(state) {
        state.formFields.id = '';
        state.formFields.name = '';
        state.formFields.email = '';
        state.formFields.dob = '';
        state.formFields.gender = 'male';
        state.formFields.points = 0;
        state.formFields.spent = 0;
        state.formFields.balance = 0;
        state.formFields.loyalty_number = '';
        state.formFields.card_barcode = '';
        state.formFields.card_barcode_1 = '';
        state.formFields.start_date = '';
        state.errors = new Error({});
    },
}
