import mutations from './mutations';
import getters from './getters';
import Error from "../../../helpers/Error";

const module = {
    namespaced: true,
    state() {
        return {
            formFields: {
                id: '',
                name:'',
                email:'',
                dob:'',
                gender:'male',
                points:0,
                spent:0,
                balance:0,
                loyalty_number:'',
                card_barcode:'',
                card_barcode_1:'',
                start_date:'',
            },
            errors: new Error({}),
        }
    },
    mutations,
    getters,
};

export default module;
