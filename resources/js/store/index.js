import state from "./state";
import getters from "./getters";
import actions from "./actions";
import mutations from "./mutations";
import earningPoints from "./modules/earningPoints/module";
import customers from "./modules/customers/module";
import customerLevel from "./modules/customer-level/module";
import campaign from "./modules/campaign/module";
import displaySetting from "./modules/displaySetting/module";

const si = {
    namespaced:true,
    modules: {
      earningPoints:earningPoints,
      customers:customers,
      customerLevel:customerLevel,
      campaign:campaign,
      displaySetting:displaySetting,
    },
    state,
    getters,
    actions,
    mutations,
};
export default si;
