
export default {
    postForm(context, {inputs, URL, method, header = {} }) {
        let headers = {_method: method};

        if(header.length)
            headers = _.concat(headers,header);

        return new Promise((resolve, reject) => {
            axios({
                method: method,
                url: URL,
                data: inputs,
                headers: headers
            }).then(response => {
                resolve(response);
            }).catch(error => {
                reject(error.response);
            }).finally(function () {

            });
        });
    },
    getEntity(context, {inputs, URL, method, header = {} }) {

        let headers = {_method: method};

        if(header.length)
            headers = _.concat(headers,header);

        return new Promise((resolve, reject) => {
            axios({
                method: method,
                url: URL,
                data: inputs,
                headers: headers
            }).then(response => {
                resolve(response);
            }).catch(error => {
                reject(error.response);
            }).finally(function () {

            });
        });
    },
}
