require('./bootstrap');
window.Vue = require('vue');

import moment from 'moment';
window.moment = moment;

import Loading from 'vue-loading-overlay';
// Import stylesheet
import 'vue-loading-overlay/dist/vue-loading.css';
// Init plugin
Vue.use(Loading);

import VueToast from 'vue-toast-notification';
// Import one of available themes
import 'vue-toast-notification/dist/theme-default.css';
//import 'vue-toast-notification/dist/theme-sugar.css';

Vue.use(VueToast,{
    position: 'bottom'
});


import helpers from './helpers/mixins.js';
const plugin = {
    install () {
        Vue.prototype.$helpers = helpers
    }
}

Vue.use(plugin);

import Vuex from 'vuex';
import si from './store/index.js';
window.Vue.use(Vuex);
window.store = new Vuex.Store({
    strict: true,
    modules: { si },
});

Vue.component('dashboard', require('./components/dashboard/Master').default);
Vue.component('earning-points', require('./components/earningPoints/Master').default);
Vue.component('customers', require('./components/customers/Master').default);
Vue.component('customer-level', require('./components/customer-level/Master').default);
Vue.component('campaign', require('./components/campaign/Master').default);
Vue.component('display-setting', require('./components/displaySetting/Master').default);


const app = new Vue({
    el: '#app',
    store,
});
