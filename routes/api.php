<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api'], function () {

    Route::group(['prefix' => '{shopifyShop}'], function () {
        Route::get("/widget", "Widget\WidgetController@index");
        Route::get("/widget/dashboard", "Widget\WidgetController@dashboard");
        Route::get("/widget/customer-join", "Widget\WidgetController@customerJoin")->name('customer.join');
        Route::get("/widget/customer-discount", "Widget\WidgetController@customerDiscount")->name('customer.discount');
    });

    /*Route::get('', 'CustomerController@index');
    Route::get('info', 'CustomerController@info')->name('customer.learnmore');
    Route::get('join', 'CustomerController@join')->name('customer.join');
    Route::get('home', 'CustomerController@home')->name('customer.index');*/

    /*Route::group(['middleware' => ['web', 'customer']], function(){
        Route::get('dashboard', 'CustomerController@dashboard')->name('customer.dashboard');
        Route::get('earn', 'CustomerController@earn')->name('customer.earn');
        Route::get('earnings', 'CustomerController@earnings')->name('customer.earnings');
        Route::get('spend', 'CustomerController@spend')->name('customer.spend');
        Route::get('spendings', 'CustomerController@spendings')->name('customer.spendings');
        Route::get('spendings/{id}', 'CustomerController@spendingDetails')->name('spending.details');

        Route::get('add-barcode', 'CustomerController@addBarcode')->name('add.barcode');
        Route::get('link-barcode', 'CustomerController@linkBarcode')->name('link.barcode');

        Route::get('cancellation', 'CustomerController@cancellation')->name('customer.cancellation');
        Route::get('cancel', 'CustomerController@cancel')->name('customer.cancel');
    });*/
});
