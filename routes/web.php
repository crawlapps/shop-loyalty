<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(["middleware" => ["web"]], function(){
    Route::get('test', 'TestController@index');
    Route::view('installation-guide', 'pages.installation_guide.index')->name('installation_guide');

    /* Plan and Pricing */
    Route::group(["middleware" => ["auth.shopify"],"prefix" => "choose-plan"], function(){
        Route::get("/", "ChoosePlan\ChoosePlanController@index")->name('choose.plan.index');
        Route::get("/free", "ChoosePlan\ChoosePlanController@chooseFreePlan")->name('choose.plan.free');
    });

    Route::group(["middleware" => ["auth.shopify", "check.plan"]], function() {

        /* Get Shopify Discount Codes */
        Route::get("/get-discount-codes", function(Request $request){
            $discounts = getDiscountCodes($request);
            return response($discounts,Illuminate\Http\Response::HTTP_OK);
        })->name('discount.code');

        /* Dashboard */
        Route::get("/","Dashboard\DashboardController@index")->name('home');

        /* Earning Points / Rules */
        Route::group(["prefix" => "earning-points"], function() {
            Route::get("/","EarningPoints\EarningPointsController@index")->name('earning-points.index');
            Route::get("/edit/{id}","EarningPoints\EarningPointsController@edit")->name('earning-points.edit');
            Route::post("/update/{id}","EarningPoints\EarningPointsController@update")->name('earning-points.update');
        });

        /* Customers */
        Route::group(["namespace"=> "Customers"], function() {
            Route::resource("/customers","CustomersController");
        });

        /* Customer Level */
        Route::group(["namespace"=> "CustomerLevel"], function() {
            Route::resource("/customer-level","CustomerLevelController");
        });

        /* Campaign  */
        Route::group(["namespace"=> "Campaign"], function() {
            Route::resource("/campaign","CampaignController");
        });

        /* Display Setting  */
        Route::group(["namespace"=> "DisplaySetting"], function() {
            Route::resource("/display-setting","DisplaySettingController");
        });

    });
});

Route::get('flush', function(){
    request()->session()->flush();
});
